package cn.hyatt.feign;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * 认证 Feign 远程调用
 *
 * @author hyatt
 */
@FeignClient(name = "auth", path = "/auth/feign")
public interface AuthFeign {
//    @GetMapping("/tokenVerify")
//    @Operation(method = "tokenVerify", summary = "token验证")
//    Boolean tokenVerify(String token);
}
