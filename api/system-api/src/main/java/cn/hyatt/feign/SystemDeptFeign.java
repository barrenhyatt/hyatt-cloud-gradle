package cn.hyatt.feign;

import cn.hyatt.dto.systemDept.SystemDeptFindDto;
import cn.hyatt.dto.systemDept.SystemDeptVo;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

/**
 * 系统部门 Feign 远程调用
 *
 * @author hyatt
 */
@FeignClient(name = "system", path = "/dept/feign")
public interface SystemDeptFeign {

    @GetMapping("/findById/{id}")
    @Operation(method = "findById", summary = "按Id查询")
    Optional<SystemDeptVo> findById(@PathVariable Long id);

    @GetMapping("/findByList")
    @Operation(method = "findByList", summary = "按列表查询")
    List<SystemDeptVo> findByList(SystemDeptFindDto dto);
}
