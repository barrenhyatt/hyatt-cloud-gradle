package cn.hyatt.feign;

import cn.hyatt.dto.systemRole.SystemRoleFindDto;
import cn.hyatt.dto.systemRole.SystemRoleVo;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * 系统角色 Feign 远程调用
 *
 * @author hyatt
 */
@FeignClient(name = "system", path = "/role/feign")
public interface SystemRoleFeign {

    @GetMapping("/findByList")
    @Operation(method = "findByList", summary = "按列表查询")
    List<SystemRoleVo> findByList(SystemRoleFindDto dto);
}
