package cn.hyatt.feign;

import cn.hyatt.dto.systemUser.SystemUserDetail;
import cn.hyatt.dto.systemUser.SystemUserFindDto;
import cn.hyatt.dto.systemUser.SystemUserVo;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

/**
 * 系统用户 Feign 远程调用
 *
 * @author hyatt
 */
@FeignClient(name = "system", path = "/user/feign")
public interface SystemUserFeign {

    @GetMapping("/findByUsername")
    @Operation(method = "findByUsername", summary = "按用户名查找")
    Optional<SystemUserDetail> findByUsername(String username);

    @GetMapping("/findById/{id}")
    @Operation(method = "findById", summary = "按ID查找")
    Optional<SystemUserVo> findById(@PathVariable Long id);

    @GetMapping("/findByList")
    @Operation(method = "findByList", summary = "按列表查找")
    List<SystemUserVo> findByList(SystemUserFindDto dto);
}
