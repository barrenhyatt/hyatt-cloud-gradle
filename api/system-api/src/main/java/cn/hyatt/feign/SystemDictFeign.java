package cn.hyatt.feign;

import cn.hyatt.dto.systemDict.SystemDictFindDto;
import cn.hyatt.dto.systemDict.SystemDictVo;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

/**
 * 系统部门 Feign 远程调用
 *
 * @author hyatt
 */
@FeignClient(name = "system", path = "/dict/feign")
public interface SystemDictFeign {

    @GetMapping("/findById/{id}")
    @Operation(method = "findById", summary = "按Id查询")
    Optional<SystemDictVo> findById(@PathVariable Long id);

    @GetMapping("/findDefaultItemByParentId")
    @Operation(method = "findDefaultItemByParentId", summary = "按父ID查找默认选项")
    Optional<SystemDictVo> findDefaultItemByParentId(Long parentId);

    @GetMapping("/findDefaultItemByCode")
    @Operation(method = "findDefaultItemByCode", summary = "按编码查找默认选项")
    Optional<SystemDictVo> findDefaultItemByCode(String code);

    @GetMapping("/findByList")
    @Operation(method = "findByList", summary = "按列表查询")
    List<SystemDictVo> findByList(SystemDictFindDto dto);
}
