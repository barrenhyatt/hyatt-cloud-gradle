package cn.hyatt.feign;

import cn.hyatt.dto.systemMenu.SystemMenuFindDto;
import cn.hyatt.dto.systemMenu.SystemMenuVo;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * 系统菜单 Feign 远程调用
 *
 * @author hyatt
 */
@FeignClient(name = "system", path = "/menu/feign")
public interface SystemMenuFeign {

    @GetMapping("/findByList")
    @Operation(method = "findByList", summary = "按列表查询")
    List<SystemMenuVo> findByList(SystemMenuFindDto dto);
}
