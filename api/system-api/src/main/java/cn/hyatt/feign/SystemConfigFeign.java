package cn.hyatt.feign;

import cn.hyatt.dto.systemConfig.SystemConfigFindDto;
import cn.hyatt.dto.systemConfig.SystemConfigVo;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

/**
 * 系统部门 Feign 远程调用
 *
 * @author hyatt
 */
@FeignClient(name = "system", path = "/config/feign")
public interface SystemConfigFeign {

    @GetMapping("/findById/{id}")
    @Operation(method = "findById", summary = "按Id查询")
    Optional<SystemConfigVo> findById(@PathVariable Long id);

    @GetMapping("/findByName")
    @Operation(method = "findByName", summary = "按名称查询")
    Optional<SystemConfigVo> findByName(String name);

    @GetMapping("/findByCode")
    @Operation(method = "findByCode", summary = "按编码查询")
    Optional<SystemConfigVo> findByCode(String code);

    @GetMapping("/findByList")
    @Operation(method = "findById", summary = "查询列表")
    List<SystemConfigVo> findByList(SystemConfigFindDto dto);
}
