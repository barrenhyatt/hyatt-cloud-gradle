package cn.hyatt.dto.systemUser;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统用户详细信息
 *
 * @author Hyatt
 */
@Getter
@Setter
public class SystemUserDetail implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "id", title = "主键", description = "表唯一主键")
    private Long id;

    @Schema(name = "createTime", title = "创建时间", description = "数据创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", title = "更新时间", description = "数据更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @Schema(name = "username", title = "用户名", description = "用户名")
    private String username;

    @Schema(name = "password", title = "用户密码", description = "用户密码")
    private String password;

    @Schema(name = "deptId", title = "部门Id", description = "用户所属部门Id")
    private String deptId;

    @Schema(name = "nickname", title = "用户昵称", description = "用户昵称")
    private String nickname;

    @Schema(name = "phone", title = "用户电话", description = "用户电话")
    private String phone;

    @Schema(name = "enabled", title = "是否启用", description = "是否启用")
    private String enabled;
}
