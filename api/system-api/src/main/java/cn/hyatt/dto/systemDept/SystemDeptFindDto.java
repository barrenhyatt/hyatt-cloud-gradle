package cn.hyatt.dto.systemDept;

import cn.hyatt.dto.PaginationDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.time.LocalDateTime;

/**
 * 系统部门 查询DTO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemDeptFindDto extends PaginationDto {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "createTime", title = "创建时间", description = "数据创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", title = "更新时间", description = "数据更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @Schema(name = "parentId", title = "父部门Id", description = "归属上级部门的Id")
    private Long parentId;

    @Schema(name = "relationChain", title = "部门关系链", description = "归属上级部门的关系链")
    private Long relationChain;

    @Schema(name = "name", title = "部门名称", description = "部门名称")
    private String name;

    @Schema(name = "code", title = "部门代码", description = "部门代码")
    private String code;

    @Schema(name = "manager", title = "管理者/负责人", description = "部门管理者")
    private String manager;

    @Schema(name = "phone", title = "管理者/负责人电话", description = "部门管理者的电话")
    private String phone;

    @Schema(name = "sort", title = "排序", description = "排序:从小到大")
    private Integer sort;

    @Schema(name = "enabled", title = "是否启用", description = "是否启用部门")
    private Boolean enabled;
}
