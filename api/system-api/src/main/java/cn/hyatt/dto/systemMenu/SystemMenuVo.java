package cn.hyatt.dto.systemMenu;

import cn.hyatt.enums.MenuType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统菜单 VO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemMenuVo implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "id", title = "主键", description = "表唯一主键")
    private Long id;

    @Schema(name = "createTime", title = "创建时间", description = "数据创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", title = "更新时间", description = "数据更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @Schema(name = "parentId", title = "父菜单Id", description = "归属上级父菜的Id")
    private Long parentId;

    @Schema(name = "type", title = "菜单类型", description = "菜单类型")
    private MenuType type;

    @Schema(name = "title", title = "菜单标题", description = "菜单标题")
    private String title;

    @Schema(name = "icon", title = "菜单图标", description = "菜单图标")
    private String icon;

    @Schema(name = "visible", title = "是否显示", description = "是否显示：true显示 false隐藏")
    private Boolean visible;

    @Schema(name = "sort", title = "排序", description = "排序")
    private Integer sort;

    @Schema(name = "path", title = "路由地址", description = "路由地址")
    private String path;

    @Schema(name = "name", title = "路由名称", description = "路由名称")
    private String name;

    @Schema(name = "component", title = "组件路径", description = "组件路径")
    private String component;

    @Schema(name = "link", title = "是否为外链", description = "是否为外链：true是 false否")
    private Boolean link;

    @Schema(name = "redirect", title = "重定向", description = "重定向")
    private String redirect;

    @Schema(name = "permissions", title = "权限字符串", description = "权限字符串")
    private String permissions;

    @Schema(name = "enabled", title = "是否启用", description = "是否启用：true启用 false未启用")
    private Boolean enabled;

    @Schema(name = "children", title = "子菜单", description = "子菜单")
    private List<SystemMenuVo> children = new ArrayList<>();
}
