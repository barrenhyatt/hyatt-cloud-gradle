package cn.hyatt.dto.systemDict;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统配置 VO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemDictVo implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "id", title = "主键", description = "表唯一主键")
    private Long id;

    @Schema(name = "createTime", title = "创建时间", description = "数据创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", title = "更新时间", description = "数据更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @Schema(name = "parentId", title = "字典类型", description = "归属上级字典类型的Id")
    private Long parentId;

    @NotNull
    @Schema(name = "name", title = "字典名称", description = "系统字典名称")
    private String name;

    @NotNull
    @Schema(name = "code", title = "字典编码", description = "系统字典编码")
    private String code;

    @Schema(name = "title", title = "字典项标题", description = "字典项标题")
    private String title;

    @Schema(name = "key", title = "字典项Key", description = "系统字典项的Key")
    private String key;

    @Schema(name = "value", title = "字典项值", description = "系统字典项的值")
    private String value;

    @Schema(name = "defaultOption", title = "字典项默认值", description = "系统字典项默认值,同一字典类只有一个默认值")
    private Boolean defaultOption;

    @Schema(name = "description", title = "字典描述", description = "字典描述")
    private String description;
}
