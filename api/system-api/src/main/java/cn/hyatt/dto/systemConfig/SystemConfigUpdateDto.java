package cn.hyatt.dto.systemConfig;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;


/**
 * 系统配置 更新DTO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemConfigUpdateDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "name", title = "配置名称", description = "系统配置名称")
    private String name;

    @NotNull
    @Schema(name = "code", title = "配置编码", description = "系统配置编码")
    private String code;

    @NotNull
    @Schema(name = "value", title = "配置值", description = "系统配置值")
    private String value;

    @Schema(name = "description", title = "配置描述", description = "配置描述")
    private String description;
}
