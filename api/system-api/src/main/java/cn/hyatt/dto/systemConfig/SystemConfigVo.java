package cn.hyatt.dto.systemConfig;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * 系统配置 VO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemConfigVo implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "id", title = "主键", description = "表唯一主键")
    private Long id;

    @Schema(name = "createTime", title = "创建时间", description = "数据创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", title = "更新时间", description = "数据更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @Schema(name = "name", title = "配置名称", description = "系统配置名称")
    private String name;

    @NotNull
    @Schema(name = "code", title = "配置编码", description = "系统配置编码")
    private String code;

    @NotNull
    @Schema(name = "value", title = "配置值", description = "系统配置值")
    private String value;

    @Schema(name = "description", title = "配置描述", description = "配置描述")
    private String description;
}
