package cn.hyatt.dto.systemDict;

import cn.hyatt.dto.PaginationDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * 系统字典 查询DTO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemDictFindDto extends PaginationDto {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "parentId", title = "字典类型", description = "归属上级字典类型的Id")
    private Long parentId;

    @NotNull
    @Schema(name = "name", title = "字典名称", description = "系统字典名称")
    private String name;

    @NotNull
    @Schema(name = "code", title = "字典编码", description = "系统字典编码")
    private String code;

}
