package cn.hyatt.dto.systemUser;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 系统用户 保存DTO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemUserSaveDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;

    @NotNull
    @Schema(name = "username", title = "用户名", description = "用户名")
    private String username;

    @NotNull
    @Schema(name = "password", title = "用户密码", description = "用户密码")
    private String password;

    @NotNull
    @Schema(name = "deptId", title = "部门Id", description = "用户所属部门Id")
    private String deptId;

    @NotNull
    @Schema(name = "nickname", title = "用户昵称", description = "用户昵称")
    private String nickname;

    @Schema(name = "phone", title = "用户电话", description = "用户电话")
    private String phone;

    @NotNull
    @Schema(name = "roleList",title = "角色列表",description = "用户关联的角色列表")
    private List<Long> roleList;
}
