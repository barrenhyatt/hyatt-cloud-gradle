package cn.hyatt.dto.systemUser;

import cn.hyatt.dto.PaginationDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.time.LocalDateTime;

/**
 * 系统用户 查询DTO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemUserFindDto extends PaginationDto {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "createTime", title = "创建时间", description = "数据创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", title = "更新时间", description = "数据更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @Schema(name = "username", title = "用户名", description = "用户名")
    private String username;

    @Schema(name = "deptId", title = "部门Id", description = "用户所属部门Id")
    private String deptId;

    @Schema(name = "nickname", title = "用户昵称", description = "用户昵称")
    private String nickname;

    @Schema(name = "phone", title = "用户电话", description = "用户电话")
    private String phone;
}
