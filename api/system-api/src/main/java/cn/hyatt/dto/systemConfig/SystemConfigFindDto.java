package cn.hyatt.dto.systemConfig;

import cn.hyatt.dto.PaginationDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;


/**
 * 系统配置 查询DTO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemConfigFindDto extends PaginationDto {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "name", title = "配置名称", description = "系统配置名称")
    private String name;

    @Schema(name = "code", title = "配置编码", description = "系统配置编码")
    private String code;
}
