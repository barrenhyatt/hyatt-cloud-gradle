package cn.hyatt.dto.systemRole;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 系统角色 保存DTO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemRoleSaveDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;

    @NotNull
    @Schema(name = "name", title = "角色名称", description = "角色名称")
    private String name;

    @NotNull
    @Schema(name = "code", title = "角色代码", description = "角色代码")
    private String code;

    @Schema(name = "description", title = "角色描述", description = "角色描述")
    private String description;

    @NotNull
    @Schema(name = "menuList",title = "菜单列表",description = "角色关联的菜单列表")
    private List<Long> menuList;
}
