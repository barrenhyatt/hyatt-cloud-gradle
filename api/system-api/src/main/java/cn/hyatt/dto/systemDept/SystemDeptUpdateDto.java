package cn.hyatt.dto.systemDept;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * 系统部门 更新DTO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemDeptUpdateDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "parentId", title = "父部门Id", description = "归属上级部门的Id")
    private Long parentId;

    @Schema(name = "name", title = "部门名称", description = "部门名称")
    private String name;

    @Schema(name = "code", title = "部门代码", description = "部门代码")
    private String code;

    @Schema(name = "manager", title = "管理者/负责人", description = "部门管理者")
    private String manager;

    @Schema(name = "phone", title = "管理者/负责人电话", description = "部门管理者的电话")
    private String phone;

    @Schema(name = "sort", title = "排序", description = "排序:从小到大")
    private Integer sort;

    @Schema(name = "enabled", title = "是否启用", description = "是否启用部门")
    private Boolean enabled;
}
