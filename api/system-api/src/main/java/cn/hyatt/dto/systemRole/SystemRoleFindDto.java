package cn.hyatt.dto.systemRole;

import cn.hyatt.dto.PaginationDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.time.LocalDateTime;

/**
 * 系统角色 查询DTO对象
 *
 * @author hyatt
 */
@Getter
@Setter
public class SystemRoleFindDto extends PaginationDto {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(name = "createTime", title = "创建时间", description = "数据创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", title = "更新时间", description = "数据更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @Schema(name = "name", title = "角色名称", description = "角色名称")
    private String name;

    @Schema(name = "code", title = "角色代码", description = "角色代码")
    private String code;

    @Schema(name = "description", title = "角色描述", description = "角色描述")
    private String description;
}
