package cn.hyatt.constant;

/**
 * 系统配置编码
 */
public interface SystemConfigConstant {
    /**
     * 是否启用验证码验证
     * 配置值类型：布尔值
     */
    String IS_ENABLE_CAPTCHA_VERIFY = "system:auth:isEnableCaptchaVerify";
    /**
     * 区分大小写的验证码
     * 配置值类型：布尔值
     */
    String IS_CASE_SENSITIVE_CAPTCHA = "system:auth:isCaseSensitiveCaptcha";
    /**
     * 验证码长度
     * 配置值类型：整型
     */
    String CAPTCHA_LENGTH = "system:auth:captchaLength";
    /**
     * 用户默认密码
     * 配置值类型：字符串
     */
    String USER_DEFAULT_PASSWORD = "system:user:defaultPassword";

}
