package cn.hyatt.event;

import cn.hyatt.dto.systemUser.SystemUserSaveDto;
import cn.hyatt.dto.systemUser.SystemUserUpdateDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 系统用户事件
 *
 * @author hyatt
 */
public interface SystemUserEvent {

    /**
     * 保存事件
     */
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    class Save {
        @Schema(name = "id", title = "保存的对象Id", description = "保存的对象Id")
        private Long id;

        @Schema(name = "saveDto", title = "保存DTO", description = "保存DTO")
        private SystemUserSaveDto saveDto;
    }

    /**
     * 更新事件
     */
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    class Update {
        @Schema(name = "", title = "", description = "")
        private Long id;

        @Schema(name = "", title = "", description = "")
        private SystemUserUpdateDto updateDto;
    }

    /**
     * 删除事件
     */
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    class Delete {
        @Schema(name = "", title = "", description = "")
        private Long id;
    }
}
