package cn.hyatt.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 菜单类型
 *
 * @author hyatt
 */
@Getter
@AllArgsConstructor
public enum MenuType {
    DIRECTORY("directory","目录","菜单目录"),
    MENU("menu","菜单","菜单"),
    BUTTON("button","按钮","按钮"),
    ;
    private final String code;
    private final String name;
    private final String remark;
}
