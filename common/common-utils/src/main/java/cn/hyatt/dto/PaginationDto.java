package cn.hyatt.dto;

import lombok.Getter;
import lombok.Setter;
import java.io.Serial;
import java.io.Serializable;


/**
 * 分页 Dto
 *
 * @author hyatt
 */
@Getter
@Setter
public class PaginationDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;


    /**
     * 当前页
     */
    private Long currentPage;

    /**
     * 页数量
     */
    private Long pageSize;

}
