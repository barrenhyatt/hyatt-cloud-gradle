package cn.hyatt.dto;

import jakarta.mail.Message;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.List;
import java.util.Properties;

/**
 * 发送邮件 DTO
 *
 * @author hyatt
 */

@Getter
@Setter
@Builder
public class SendMailDto {
    /**
     * 配置信息
     */
    private Properties properties;

    /**
     * 邮件头信息
     */
//    private Map<String,String> headers;

    /**
     * 发件人电子邮件
     */
    private String senderEmail;

    /**
     * 发件人密码
     */
    private String senderPassword;

    /**
     * 收件人类型
     */
    private Message.RecipientType recipientType;

    /**
     * 收件人列表
     */
    private List<String> recipientList;

    /**
     * 主题
     */
    private String subject;

    /**
     * 邮件内容
     */
    private String content;

    /**
     * 附件文件名
     */
    private String attachmentFileName;

    /**
     * 附件文件
     */
    private File attachmentFile;

}
