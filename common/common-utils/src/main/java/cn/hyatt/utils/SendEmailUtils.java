package cn.hyatt.utils;

import cn.hyatt.dto.SendMailDto;
import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;
import jakarta.mail.*;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.List;
import java.util.Properties;
/**
 * 发送电子邮件 工具类
 *
 * @author hyatt
 */
@Slf4j
public final class SendEmailUtils {

    /**
     * 发送邮件
     *
     * @param dto 发送邮件对象
     * @return 是否发送成功
     */
    public static boolean sendMail(SendMailDto dto) {

        // 创建Session对象，用于与邮件服务器进行通信
        Session session = Session.getInstance(dto.getProperties(), new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(dto.getSenderEmail(), dto.getSenderPassword());
            }
        });

        try {
            // 创建MimeMessage对象，并设置发件人、收件人、主题和内容
            MimeMessage message = new MimeMessage(session);

            // 设置发件人地址
            message.setFrom(new InternetAddress(dto.getSenderEmail()));
            // 设置收件人
            for (String address : dto.getRecipientList()) {
                message.addRecipient(dto.getRecipientType(), new InternetAddress(address));
            }
            // 设置主题
            message.setSubject(dto.getSubject());

            // 设置邮件内容
            BodyPart textPart = new MimeBodyPart();
            textPart.setText(dto.getContent());

            // 添加附件部分
            BodyPart attachmentPart = null;
            if (dto.getAttachmentFile() != null) {
                attachmentPart = new MimeBodyPart();
                attachmentPart.setFileName(dto.getAttachmentFileName() != null ? dto.getAttachmentFileName() : dto.getAttachmentFile().getName());
                attachmentPart.setDataHandler(new DataHandler(new FileDataSource(dto.getAttachmentFile())));
            }


            MimeMultipart mimeMultipart = new MimeMultipart();
            mimeMultipart.addBodyPart(textPart);
            if (attachmentPart != null) {
                mimeMultipart.addBodyPart(attachmentPart);
            }

            // 设置发送内容
            message.setContent(mimeMultipart);

            // 发送邮件
            Transport.send(message);
            return true;
        } catch (MessagingException e) {
            return false;
        }
    }

    /**
     * 获取 QQ SSL连接配置
     *
     * @return SSL连接配置
     */
    public static Properties getQqProperties() {
        // SMTP服务器配置
        final String host = "smtp.qq.com";
        final String port = "465";
        return getProperties(host, port);
    }

    /**
     * 获取 163 SSL连接配置
     *
     * @return SSL连接配置
     */
    public static Properties get163Properties() {
        // SMTP服务器配置
        final String host = "smtp.163.com";
        final String port = "465";
        return getProperties(host, port);
    }

    /**
     * SSL连接配置 对象
     *
     * @param host 地址
     * @param port 端口
     * @return SSL连接配置
     */
    public static Properties getProperties(String host, String port) {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.ssl.enable", "true");
        return properties;
    }

    public static void main(String[] args) {
        File file = new File("D:\\Code\\Java\\lh-abs-v3\\doc\\测试专用文件.txt");
        SendMailDto dto = SendMailDto.builder()
                .properties(SendEmailUtils.getQqProperties())
                .senderEmail("1666090780@qq.com")
                .senderPassword("vfrjgjvcnuvtfahd")
                .recipientType(Message.RecipientType.TO)
                .recipientList(List.of("3150315757@qq.com"))
                .subject("测试-测试")
                .content("测试内容")
                .attachmentFileName("测试文件.txt")
                .attachmentFile(file)
                .build();
        boolean sentMail = SendEmailUtils.sendMail(dto);
        System.out.println(sentMail);
    }
}
