package cn.hyatt.utils;

import java.nio.charset.StandardCharsets;

/**
 * 字节转换工具
 *
 * @author hyatt
 */
public class ByteConvertUtils {

    /**
     * 转换 int 高位在前
     *
     * @param data 数据
     * @return 返回结果
     */
    public static int toIntHigh(byte[] data) {
        int result = 0;

        for (byte datum : data) {
            result <<= 8;  // 将结果向左移动8位，给下一个字节腾出位置
            result |= datum & 0xFF;  // 使用位或运算符将当前字节与结果进行合并
        }

        return result;
    }

    /**
     * 转换 int 高位在后
     *
     * @param data 数据
     * @return 返回结果
     */
    public static int toIntLow(byte[] data) {
        int result = 0;
        for (int i = data.length - 1; i >= 0; i--) {
            result <<= 8;  // 将结果向左移动8位，给下一个字节腾出位置
            result |= data[i] & 0xFF;  // 使用位或运算符将当前字节与结果进行合并
        }
        return result;
    }

    /**
     * byte 到十六进制字符串
     *
     * @param data 字节
     * @return 返回结果
     */
    public static String byteToHexString(byte data) {
        return String.format("%02X", data);
    }

    /**
     * 转换 String
     *
     * @param data 数据
     * @return 返回结果
     */
    public static String toStr(byte[] data) {
        return new String(data, StandardCharsets.UTF_8);
    }

    public static void main(String[] args) {
        System.out.println(byteToHexString((byte)0xAB));
    }


}
