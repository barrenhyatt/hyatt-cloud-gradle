package cn.hyatt.utils;

/**
 * 正则匹配工具
 *
 * @author hyatt
 */
public final class RegexMatchUtils {
    /**
     * 字符串 IPv4 正则匹配
     *
     * @param text 匹配的字符串
     * @return true 符合 false 不符合
     */
    public static boolean ip4(String text) {
        final String regexPattern = "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
        return text.matches(regexPattern);
    }

    /**
     * 字符串 IPv6 正则匹配
     *
     * @param text 匹配的字符串
     * @return true 符合 false 不符合
     */
    public static boolean ip6(String text) {
        final String regexPattern = "^([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$";
        return text.matches(regexPattern);
    }

    /**
     * 字符串 Email 正则匹配
     *
     * @param text 匹配的字符串
     * @return true 符合 false 不符合
     */
    public static boolean email(String text) {
        final String regexPattern = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
        return text.matches(regexPattern);
    }

    /**
     * 字符串 中国手机号码 正则匹配
     *
     * @param text 匹配的字符串
     * @return true 符合 false 不符合
     */
    public static boolean chinaMobilePhoneNumber(String text) {
        final String regexPattern = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$";
        return text.matches(regexPattern);
    }

    public static void main(String[] args) {
        System.out.println(ip4("1.1.1.1"));
    }
}
