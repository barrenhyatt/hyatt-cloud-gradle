package cn.hyatt.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * 日期时间工具
 *
 * @author hyatt
 */
public class DateTimeUtils {
    // 常用日期时间格式
    public static final String COMMON_DATE_TIME_FORM = "yyyy-MM-dd HH:mm:ss";

    // 常用日期格式
    public static final String COMMON_DATE_FORM = "yyyy-MM-dd";

    // 常用时间格式
    public static final String COMMON_TIME_FORM = "HH:mm:ss";

    /**
     * Date 转 LocalDateTime
     * @param date 日期
     * @return 结果
     */
    public static LocalDateTime dateToLocalDateTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();
        return instant.atZone(zoneId).toLocalDateTime();
    }

    /**
     * LocalDateTime 转 Date
     * @param dateTime LocalDateTime
     * @return 结果
     */
    public static Date localDateTimeToDate(LocalDateTime dateTime) {
        Instant instant = dateTime.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }
}
