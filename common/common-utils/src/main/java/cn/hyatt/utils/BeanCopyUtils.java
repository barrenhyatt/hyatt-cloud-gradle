package cn.hyatt.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Copy 工具
 *
 * @author 1666090780
 */
public class BeanCopyUtils {


    /**
     * copy 属性 -忽略源数据内空的字段
     *
     * @param source 源数据
     * @param target 目标
     */
    public static void copyProperties(Object source, Object target) {
        String[] ignore = new String[]{};
        // copy 属性
        copyProperties(source, target, ignore);
    }

    /**
     * copy 属性 -忽略源数据内空的字段
     *
     * @param source 源数据
     * @param target 目标
     * @param ignore 忽略字段
     */
    public static void copyProperties(Object source, Object target, String... ignore) {
        // copy 属性
        copyProperties(source, target, true, ignore);
    }

    /**
     * copy 属性 -忽略源数据内空的字段
     *
     * @param source     源数据
     * @param target     目标
     * @param ignoreNull 忽略Null
     * @param ignore     忽略字段
     */
    public static void copyProperties(Object source, Object target, Boolean ignoreNull, String... ignore) {
        // 为空的字段
        String[] nullPropertyNames = getNullPropertyNames(source);
        // copy 属性
        BeanUtils.copyProperties(source, target, nullPropertyNames);
    }

    private static String[] getNullPropertyNames(Object source, String... ignore) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>(List.of(ignore));
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
}
