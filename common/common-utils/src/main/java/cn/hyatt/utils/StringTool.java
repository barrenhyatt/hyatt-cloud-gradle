package cn.hyatt.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * 字符串工具
 *
 * @author hyatt
 */
public class StringTool {

    /**
     * 删除前缀
     * removePrefix("system_user","system_") --> user
     * removePrefix("system_user_name","system_") --> user_name
     * removePrefix("sys_user","system_") --> sys_user
     *
     * @param str    原字符串
     * @param prefix 前缀字符串
     * @return 返回删除前缀的字符串
     */
    public static String removePrefix(String str, String prefix) {
        if (str.startsWith(prefix) && !prefix.isBlank()) {
            int prefixSize = prefix.length();
            int strSize = str.length();
            return str.substring(prefixSize,strSize);
        } else {
            return str;
        }
    }

    /**
     * 字符串转大驼峰
     * strToUpperCamel("user_name","_") --> UserName
     * strToUpperCamel("my_user_name","_") --> MyUserName
     *
     * @param str       原字符串
     * @param separator 分隔符
     * @return 返回驼峰命名格式
     */
    public static String strToUpperCamel(String str, String separator) {
        String[] split = str.split(separator);
        StringBuilder builder = new StringBuilder();
        for (String s : split) {
            builder.append(StringUtils.capitalize(s));
        }
        return builder.toString();
    }

    /**
     * 字符串转小驼峰
     * strToUpperCamel("user_name","_") --> UserName
     * strToUpperCamel("my_user_name","_") --> MyUserName
     *
     * @param str       原字符串
     * @param separator 分隔符
     * @return 返回驼峰命名格式
     */
    public static String strToCamel(String str, String separator) {
        String[] split = str.split(separator);
        StringBuilder builder = new StringBuilder(split[0]);
        for (int i = 1; i < split.length; i++) {
            builder.append(StringUtils.capitalize(split[i]));
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        System.out.println(removePrefix("system_user_name", "system_"));
    }

}
