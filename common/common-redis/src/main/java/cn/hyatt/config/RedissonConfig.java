package cn.hyatt.config;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {
    @Value("${spring.data.redis.host}")
    private String redisHost;

    @Value("${spring.data.redis.port}")
    private int redisPort;

    @Bean
    public RedissonClient redissonClient() {
        // 配置对象
        Config config = new Config();
        // 单体架构服务-连接地址设置
        config.useSingleServer()
                .setAddress("redis://" + redisHost + ":" + redisPort);
        // 设置序列化方式
        JsonJacksonCodec codec = new JsonJacksonCodec();
        //补充支持jsr310
        codec.getObjectMapper()
                .registerModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        config.setCodec(codec);
        // 创建RedissonClient
        return Redisson.create(config);
    }
}
