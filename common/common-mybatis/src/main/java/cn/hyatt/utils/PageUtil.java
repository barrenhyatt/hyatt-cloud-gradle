package cn.hyatt.utils;

import cn.hyatt.dto.PaginationDto;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 分页工具
 *
 * @author 16660906780
 */
public class PageUtil {

    /**
     * 分页对象
     * @param dto 继承 PageableDto 的对象
     * @return page
     */
    public static <DTO extends PaginationDto, CLASS> Page<CLASS> page(DTO dto) {
        return new Page<>(dto.getCurrentPage(), dto.getPageSize());
    }

}
