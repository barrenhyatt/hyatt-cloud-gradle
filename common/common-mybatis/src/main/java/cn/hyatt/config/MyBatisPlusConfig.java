package cn.hyatt.config;

import cn.hyatt.handler.BaseEntityObjectHandler;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MyBatisPlus 配置类
 *
 * @author hyatt
 */
@Configuration
public class MyBatisPlusConfig {
    /**
     * MyBatisPlus 插件
     *
     * @return 返回 MyBatisPlus 插件对象
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        // 创建MyBatisPlus拦截器对象
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 添加分页拦截器
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return interceptor;
    }

    /**
     * 自动填充对象处理
     *
     * @return 处理对象
     */
    @Bean
    public MetaObjectHandler metaObjectHandler() {
        return new BaseEntityObjectHandler();
    }
}
