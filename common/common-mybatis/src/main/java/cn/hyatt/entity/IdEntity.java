package cn.hyatt.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
public class IdEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 42L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

}
