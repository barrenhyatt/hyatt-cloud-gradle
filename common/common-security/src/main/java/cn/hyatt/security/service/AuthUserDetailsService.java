package cn.hyatt.security.service;

import cn.hyatt.dto.systemUser.SystemUserDetail;
import cn.hyatt.feign.SystemUserFeign;
import cn.hyatt.security.entity.LoginUserDetails;
import cn.hyatt.utils.BeanCopyUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * 身份验证用户详细信息服务
 *
 * @author hyatt
 */
@Slf4j
@Service
@Primary
@AllArgsConstructor
public class AuthUserDetailsService implements UserDetailsService {

    private SystemUserFeign systemUserFeign;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<SystemUserDetail> optional = systemUserFeign.findByUsername(username);
        SystemUserDetail systemUserDetail = optional.orElseThrow(() -> new UsernameNotFoundException("找不到用户名(" + username + ")的数据"));
        LoginUserDetails userDetails = new LoginUserDetails();
        BeanCopyUtils.copyProperties(systemUserDetail, userDetails);
        return userDetails;
    }
}
