package cn.hyatt.security.utils;

import cn.hyatt.security.entity.LoginUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

    /**
     * 获取安全上下文
     *
     * @return SecurityContext
     */
    public static SecurityContext getSecurityContext() {
        return SecurityContextHolder.getContext();
    }

    /**
     * 获取身份验证信息
     *
     * @return Authentication
     */
    public static Authentication getAuthentication() {
        return getSecurityContext().getAuthentication();
    }

    /**
     * 获取登录用户详情
     *
     * @return LoginUserDetails
     */
    public static LoginUserDetails getLoginUserDetails() {
        try {
            return (LoginUserDetails) getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new RuntimeException("无法获取到登录用户详情");
        }
    }

    /**
     * 获取用户Id
     *
     * @return 用户Id
     */
    public static Long getUserId() {
        return getLoginUserDetails().getId();
    }
}
