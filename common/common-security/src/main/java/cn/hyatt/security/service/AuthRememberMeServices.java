package cn.hyatt.security.service;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.RememberMeServices;

/**
 * 身份验证“记住我”服务
 *
 * @author hyatt
 */
@Slf4j
public class AuthRememberMeServices implements RememberMeServices {
    @Override
    public Authentication autoLogin(HttpServletRequest request, HttpServletResponse response) {
        log.info("AuthRememberMeServices:autoLogin");
        return null;
    }

    @Override
    public void loginFail(HttpServletRequest request, HttpServletResponse response) {
        log.info("AuthRememberMeServices:loginFail");
    }

    @Override
    public void loginSuccess(HttpServletRequest request, HttpServletResponse response, Authentication successfulAuthentication) {
        log.info("AuthRememberMeServices:loginSuccess");
    }
}
