package cn.hyatt.security.repository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import java.util.Date;

@Slf4j
public class AuthPersistentTokenRepository implements PersistentTokenRepository {
    @Override
    public void createNewToken(PersistentRememberMeToken token) {
        log.info("AuthPersistentTokenRepository:createNewToken");
    }

    @Override
    public void updateToken(String series, String tokenValue, Date lastUsed) {
        log.info("AuthPersistentTokenRepository:updateToken");
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        log.info("AuthPersistentTokenRepository:getTokenForSeries");
        return null;
    }

    @Override
    public void removeUserTokens(String username) {
        log.info("AuthPersistentTokenRepository:removeUserTokens");
    }
}
