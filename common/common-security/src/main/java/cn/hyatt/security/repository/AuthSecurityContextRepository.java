package cn.hyatt.security.repository;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;

@Slf4j
public class AuthSecurityContextRepository implements SecurityContextRepository {
    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
        log.info("AuthSecurityContextRepository:loadContext");
        return null;
    }

    @Override
    public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {
        log.info("AuthSecurityContextRepository:saveContext");
    }

    @Override
    public boolean containsContext(HttpServletRequest request) {
        log.info("AuthSecurityContextRepository:containsContext");
        return false;
    }
}
