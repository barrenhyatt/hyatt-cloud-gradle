package cn.hyatt.controller;

import cn.hyatt.dto.R;
import cn.hyatt.dto.systemConfig.SystemConfigFindDto;
import cn.hyatt.dto.systemConfig.SystemConfigSaveDto;
import cn.hyatt.dto.systemConfig.SystemConfigUpdateDto;
import cn.hyatt.dto.systemConfig.SystemConfigVo;
import cn.hyatt.entity.SystemConfig;
import cn.hyatt.service.SystemConfigService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 系统用户 控制器
 *
 * @author hyatt
 */
@Slf4j
@Tag(name = "系统字典",description = "系统字典")
@ResponseBody
@RestController
@RequestMapping("/config")
@AllArgsConstructor
public class SystemConfigController {

    private SystemConfigService service;

    @GetMapping("/list")
    @Operation(summary = "获取列表")
    public R<List<SystemConfigVo>> list(SystemConfigFindDto dto) {
        List<SystemConfig> list = service.findByList(dto);
        return R.ok(list.stream().map(service::toVo).toList());
    }

    @GetMapping("/page")
    @Operation(summary = "获取分页")
    public R<IPage<SystemConfigVo>> page(SystemConfigFindDto dto) {
        IPage<SystemConfig> page = service.findByPage(dto);
        IPage<SystemConfigVo> voPage = page.convert(service::toVo);
        return R.ok(voPage);
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取分页")
    public R<SystemConfigVo> getById(@PathVariable Long id) {
        Optional<SystemConfig> optional = service.getOptById(id);
        SystemConfig entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        return R.ok(service.toVo(entity));
    }

    @PostMapping
    @Operation(summary = "新增保存")
    public R<Void> save(@RequestBody @Validated SystemConfigSaveDto dto) {
        service.save(dto);
        return R.ok();
    }

    @PutMapping("/{id}")
    @Operation(summary = "更新修改")
    public R<Void> update(@PathVariable Long id, @RequestBody @Validated SystemConfigUpdateDto dto) {
        service.update(id, dto);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "删除")
    public R<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return R.ok();
    }

}
