package cn.hyatt.controller;

import cn.hyatt.dto.R;
import cn.hyatt.dto.systemMenu.SystemMenuFindDto;
import cn.hyatt.entity.SystemMenu;
import cn.hyatt.dto.systemMenu.SystemMenuSaveDto;
import cn.hyatt.dto.systemMenu.SystemMenuUpdateDto;
import cn.hyatt.dto.systemMenu.SystemMenuVo;
import cn.hyatt.service.SystemMenuService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 系统用户 控制器
 *
 * @author hyatt
 */
@Slf4j
@Tag(name = "系统菜单",description = "系统菜单")
@ResponseBody
@RestController
@RequestMapping("/menu")
@AllArgsConstructor
public class SystemMenuController {
    private SystemMenuService service;

    @GetMapping("/list")
    @Operation(summary = "获取列表")
    public R<List<SystemMenuVo>> list(SystemMenuFindDto dto) {
        List<SystemMenu> list = service.findByList(dto);
        return R.ok(list.stream().map(service::toVo).toList());
    }

    @GetMapping("/page")
    @Operation(summary = "获取分页")
    public R<IPage<SystemMenuVo>> page(SystemMenuFindDto dto) {
        IPage<SystemMenu> page = service.findByPage(dto);
        IPage<SystemMenuVo> voPage = page.convert(service::toVo);
        return R.ok(voPage);
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取分页")
    public R<SystemMenuVo> getById(@PathVariable Long id) {
        Optional<SystemMenu> optional = service.getOptById(id);
        SystemMenu entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        return R.ok(service.toVo(entity));
    }

    @PostMapping
    @Operation(summary = "新增保存")
    public R<Void> save(@RequestBody @Validated SystemMenuSaveDto dto) {
        service.save(dto);
        return R.ok();
    }

    @PutMapping("/{id}")
    @Operation(summary = "更新修改")
    public R<Void> update(@PathVariable Long id, @RequestBody @Validated SystemMenuUpdateDto dto) {
        service.update(id, dto);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "删除")
    public R<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return R.ok();
    }

}
