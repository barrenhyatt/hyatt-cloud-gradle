package cn.hyatt.controller;

import cn.hyatt.dto.R;
import cn.hyatt.dto.systemRole.SystemRoleFindDto;
import cn.hyatt.dto.systemRole.SystemRoleSaveDto;
import cn.hyatt.dto.systemRole.SystemRoleUpdateDto;
import cn.hyatt.dto.systemRole.SystemRoleVo;
import cn.hyatt.entity.SystemRole;
import cn.hyatt.service.SystemRoleService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 系统用户 控制器
 *
 * @author hyatt
 */
@Slf4j
@Tag(name = "系统角色",description = "系统角色")
@ResponseBody
@RestController
@RequestMapping("/role")
@AllArgsConstructor
public class SystemRoleController {
    private SystemRoleService service;

    @GetMapping("/list")
    @Operation(summary = "获取列表")
    public R<List<SystemRoleVo>> list(SystemRoleFindDto dto) {
        List<SystemRole> list = service.findByList(dto);
        return R.ok(list.stream().map(service::toVo).toList());
    }

    @GetMapping("/page")
    @Operation(summary = "获取分页")
    public R<IPage<SystemRoleVo>> page(SystemRoleFindDto dto) {
        IPage<SystemRole> page = service.findByPage(dto);
        IPage<SystemRoleVo> voPage = page.convert(service::toVo);
        return R.ok(voPage);
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取分页")
    public R<SystemRoleVo> getById(@PathVariable Long id) {
        Optional<SystemRole> optional = service.getOptById(id);
        SystemRole entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        return R.ok(service.toVo(entity));
    }

    @PostMapping
    @Operation(summary = "新增保存")
    public R<Void> save(@RequestBody @Validated SystemRoleSaveDto dto) {
        service.save(dto);
        return R.ok();
    }

    @PutMapping("/{id}")
    @Operation(summary = "更新修改")
    public R<Void> update(@PathVariable Long id, @RequestBody @Validated SystemRoleUpdateDto dto) {
        service.update(id, dto);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "删除")
    public R<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return R.ok();
    }

}
