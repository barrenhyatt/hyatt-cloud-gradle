package cn.hyatt.controller;

import cn.hyatt.dto.R;
import cn.hyatt.dto.systemUser.SystemUserFindDto;
import cn.hyatt.dto.systemUser.SystemUserSaveDto;
import cn.hyatt.dto.systemUser.SystemUserUpdateDto;
import cn.hyatt.dto.systemUser.SystemUserVo;
import cn.hyatt.entity.SystemUser;
import cn.hyatt.service.SystemUserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 系统用户 控制器
 *
 * @author hyatt
 */
@Slf4j
@Tag(name = "系统用户",description = "系统用户")
@ResponseBody
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class SystemUserController {
    private SystemUserService service;

    @GetMapping("/list")
    @Operation(summary = "获取列表")
    public R<List<SystemUserVo>> list(SystemUserFindDto dto) {
        List<SystemUser> list = service.findByList(dto);
        return R.ok(list.stream().map(service::toVo).toList());
    }

    @GetMapping("/page")
    @Operation(summary = "获取分页")
    public R<IPage<SystemUserVo>> page(SystemUserFindDto dto) {
        IPage<SystemUser> page = service.findByPage(dto);
        IPage<SystemUserVo> voPage = page.convert(service::toVo);
        return R.ok(voPage);
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取分页")
    public R<SystemUserVo> getById(@PathVariable Long id) {
        Optional<SystemUser> optional = service.getOptById(id);
        SystemUser entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        return R.ok(service.toVo(entity));
    }

    @PostMapping
    @Operation(summary = "新增保存")
    public R<Void> save(@RequestBody @Validated SystemUserSaveDto dto) {
        service.save(dto);
        return R.ok();
    }

    @PutMapping("/{id}")
    @Operation(summary = "更新修改")
    public R<Void> update(@PathVariable Long id, @RequestBody @Validated SystemUserUpdateDto dto) {
        service.update(id, dto);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "删除")
    public R<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return R.ok();
    }

}
