package cn.hyatt.controller;

import cn.hyatt.dto.R;
import cn.hyatt.dto.systemDept.SystemDeptSaveDto;
import cn.hyatt.dto.systemDept.SystemDeptUpdateDto;
import cn.hyatt.entity.SystemDept;
import cn.hyatt.dto.systemDept.SystemDeptFindDto;
import cn.hyatt.dto.systemDept.SystemDeptVo;
import cn.hyatt.service.SystemDeptService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 系统用户 控制器
 *
 * @author hyatt
 */
@Slf4j
@Tag(name = "系统部门",description = "系统部门")
@ResponseBody
@RestController
@RequestMapping("/dept")
@AllArgsConstructor
public class SystemDeptController {
    private SystemDeptService service;

    @GetMapping("/list")
    @Operation(summary = "获取列表")
    public R<List<SystemDeptVo>> list(SystemDeptFindDto dto) {
        List<SystemDept> list = service.findByList(dto);
        return R.ok(list.stream().map(service::toVo).toList());
    }

    @GetMapping("/page")
    @Operation(summary = "获取分页")
    public R<IPage<SystemDeptVo>> page(SystemDeptFindDto dto) {
        IPage<SystemDept> page = service.findByPage(dto);
        IPage<SystemDeptVo> voPage = page.convert(service::toVo);
        return R.ok(voPage);
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取分页")
    public R<SystemDeptVo> getById(@PathVariable Long id) {
        Optional<SystemDept> optional = service.getOptById(id);
        SystemDept entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        return R.ok(service.toVo(entity));
    }

    @PostMapping
    @Operation(summary = "新增保存")
    public R<Void> save(@RequestBody @Validated SystemDeptSaveDto dto) {
        service.save(dto);
        return R.ok();
    }

    @PutMapping("/{id}")
    @Operation(summary = "更新修改")
    public R<Void> update(@PathVariable Long id, @RequestBody @Validated SystemDeptUpdateDto dto) {
        service.update(id, dto);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "删除")
    public R<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return R.ok();
    }

}
