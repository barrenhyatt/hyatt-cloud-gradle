package cn.hyatt.controller;

import cn.hyatt.dto.R;
import cn.hyatt.dto.systemDict.SystemDictFindDto;
import cn.hyatt.dto.systemDict.SystemDictSaveDto;
import cn.hyatt.dto.systemDict.SystemDictUpdateDto;
import cn.hyatt.dto.systemDict.SystemDictVo;
import cn.hyatt.entity.SystemDict;
import cn.hyatt.service.SystemDictService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 系统用户 控制器
 *
 * @author hyatt
 */
@Slf4j
@Tag(name = "系统字典",description = "系统字典")
@ResponseBody
@RestController
@RequestMapping("/dict")
@AllArgsConstructor
public class SystemDictController {
    private SystemDictService service;

    @GetMapping("/list")
    @Operation(summary = "获取列表")
    public R<List<SystemDictVo>> list(SystemDictFindDto dto) {
        List<SystemDict> list = service.findByList(dto);
        return R.ok(list.stream().map(service::toVo).toList());
    }

    @GetMapping("/page")
    @Operation(summary = "获取分页")
    public R<IPage<SystemDictVo>> page(SystemDictFindDto dto) {
        IPage<SystemDict> page = service.findByPage(dto);
        IPage<SystemDictVo> voPage = page.convert(service::toVo);
        return R.ok(voPage);
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取分页")
    public R<SystemDictVo> getById(@PathVariable Long id) {
        Optional<SystemDict> optional = service.getOptById(id);
        SystemDict entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        return R.ok(service.toVo(entity));
    }

    @PostMapping
    @Operation(summary = "新增保存")
    public R<Void> save(@RequestBody @Validated SystemDictSaveDto dto) {
        service.save(dto);
        return R.ok();
    }

    @PutMapping("/{id}")
    @Operation(summary = "更新修改")
    public R<Void> update(@PathVariable Long id, @RequestBody @Validated SystemDictUpdateDto dto) {
        service.update(id, dto);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "删除")
    public R<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return R.ok();
    }

}
