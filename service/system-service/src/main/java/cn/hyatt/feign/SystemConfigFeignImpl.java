package cn.hyatt.feign;


import cn.hyatt.dto.systemConfig.SystemConfigFindDto;
import cn.hyatt.dto.systemConfig.SystemConfigVo;
import cn.hyatt.entity.SystemConfig;
import cn.hyatt.service.SystemConfigService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * 系统部门远程 Feign 调用实现
 *
 * @author hyatt
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping("/config/feign")
@AllArgsConstructor
public class SystemConfigFeignImpl implements SystemConfigFeign {

    private SystemConfigService service;

    @Override
    public Optional<SystemConfigVo> findById(Long id) {
        Optional<SystemConfig> opt = service.getOptById(id);
        return opt.map(service::toVo);
    }

    @Override
    public Optional<SystemConfigVo> findByName(String name) {
        Optional<SystemConfig> opt = service.getOneOpt(new LambdaQueryWrapper<SystemConfig>().eq(SystemConfig::getName, name));
        return opt.map(service::toVo);
    }

    @Override
    public Optional<SystemConfigVo> findByCode(String code) {
        Optional<SystemConfig> opt = service.getOneOpt(new LambdaQueryWrapper<SystemConfig>().eq(SystemConfig::getCode, code));
        return opt.map(service::toVo);
    }

    @Override
    public List<SystemConfigVo> findByList(SystemConfigFindDto dto) {
        List<SystemConfig> list = service.findByList(dto);
        return list.stream().map(service::toVo).toList();
    }
}
