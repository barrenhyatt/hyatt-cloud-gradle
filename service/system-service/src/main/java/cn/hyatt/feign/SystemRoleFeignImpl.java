package cn.hyatt.feign;


import cn.hyatt.dto.systemRole.SystemRoleFindDto;
import cn.hyatt.dto.systemRole.SystemRoleVo;
import cn.hyatt.entity.SystemRole;
import cn.hyatt.service.SystemRoleService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统角色远程 Feign 调用实现
 *
 * @author hyatt
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping("/role/feign")
@AllArgsConstructor
public class SystemRoleFeignImpl implements SystemRoleFeign {

    private SystemRoleService service;

    @Override
    public List<SystemRoleVo> findByList(SystemRoleFindDto dto) {
        List<SystemRole> list = service.findByList(dto);
        return list.stream().map(service::toVo).toList();
    }
}
