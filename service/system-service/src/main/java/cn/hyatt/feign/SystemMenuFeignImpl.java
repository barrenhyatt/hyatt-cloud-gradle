package cn.hyatt.feign;


import cn.hyatt.dto.systemMenu.SystemMenuFindDto;
import cn.hyatt.dto.systemMenu.SystemMenuVo;
import cn.hyatt.entity.SystemMenu;
import cn.hyatt.service.SystemMenuService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统菜单远程 Feign 调用实现
 *
 * @author hyatt
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping("/menu/feign")
@AllArgsConstructor
public class SystemMenuFeignImpl implements SystemMenuFeign {

    private SystemMenuService service;

    @Override
    public List<SystemMenuVo> findByList(SystemMenuFindDto dto) {
        List<SystemMenu> list = service.findByList(dto);
        return list.stream().map(service::toVo).toList();
    }
}
