package cn.hyatt.feign;


import cn.hyatt.dto.systemDict.SystemDictFindDto;
import cn.hyatt.dto.systemDict.SystemDictVo;
import cn.hyatt.entity.SystemDict;
import cn.hyatt.service.SystemDictService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * 系统部门远程 Feign 调用实现
 *
 * @author hyatt
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping("/dict/feign")
@AllArgsConstructor
public class SystemDictFeignImpl implements SystemDictFeign {

    private SystemDictService service;

    @Override
    public Optional<SystemDictVo> findById(Long id) {
        Optional<SystemDict> opt = service.getOptById(id);
        return opt.map(service::toVo);
    }

    @Override
    public Optional<SystemDictVo> findDefaultItemByParentId(Long parentId) {
        Optional<SystemDict> opt = service.getOneOpt(new LambdaQueryWrapper<SystemDict>()
                .eq(SystemDict::getParentId, parentId)
                .eq(SystemDict::getDefaultValue, true)
        );
        return opt.map(service::toVo);
    }

    @Override
    public Optional<SystemDictVo> findDefaultItemByCode(String code) {
        Optional<SystemDict> opt = service.getOneOpt(new LambdaQueryWrapper<SystemDict>()
                .eq(SystemDict::getCode, code)
                .eq(SystemDict::getDefaultValue, true)
        );
        return opt.map(service::toVo);
    }

    @Override
    public List<SystemDictVo> findByList(SystemDictFindDto dto) {
        List<SystemDict> list = service.findByList(dto);
        return list.stream().map(service::toVo).toList();
    }
}
