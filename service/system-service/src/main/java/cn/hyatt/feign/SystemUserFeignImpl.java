package cn.hyatt.feign;

import cn.hyatt.dto.systemUser.SystemUserDetail;
import cn.hyatt.dto.systemUser.SystemUserFindDto;
import cn.hyatt.dto.systemUser.SystemUserVo;
import cn.hyatt.entity.SystemUser;
import cn.hyatt.service.SystemUserService;
import cn.hyatt.utils.BeanCopyUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * 系统用户远程 Feign 调用实现
 *
 * @author hyatt
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping("/user/feign")
@AllArgsConstructor
public class SystemUserFeignImpl implements SystemUserFeign {

    private SystemUserService service;

    @Override
    public Optional<SystemUserDetail> findByUsername(String username) {
        Optional<SystemUser> opt = service.getOneOpt(new LambdaQueryWrapper<SystemUser>().eq(SystemUser::getUsername, username));
        return opt.map(entity -> {
            SystemUserDetail systemUserDetailDto = new SystemUserDetail();
            BeanCopyUtils.copyProperties(opt.get(), systemUserDetailDto);
            return systemUserDetailDto;
        });
    }

    @Override
    public Optional<SystemUserVo> findById(Long id) {
        Optional<SystemUser> opt = service.getOptById(id);
        return opt.map(service::toVo);
    }

    @Override
    public List<SystemUserVo> findByList(SystemUserFindDto dto) {
        List<SystemUser> list = service.findByList(dto);
        return list.stream().map(service::toVo).toList();
    }
}
