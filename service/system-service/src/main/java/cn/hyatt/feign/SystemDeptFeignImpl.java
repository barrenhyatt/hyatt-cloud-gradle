package cn.hyatt.feign;


import cn.hyatt.dto.systemDept.SystemDeptFindDto;
import cn.hyatt.dto.systemDept.SystemDeptVo;
import cn.hyatt.entity.SystemDept;
import cn.hyatt.service.SystemDeptService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * 系统部门远程 Feign 调用实现
 *
 * @author hyatt
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping("/dept/feign")
@AllArgsConstructor
public class SystemDeptFeignImpl implements SystemDeptFeign {

    private SystemDeptService service;

    @Override
    public Optional<SystemDeptVo> findById(Long id) {
        Optional<SystemDept> opt = service.getOptById(id);
        return opt.map(service::toVo);
    }

    @Override
    public List<SystemDeptVo> findByList(SystemDeptFindDto dto) {
        List<SystemDept> list = service.findByList(dto);
        return list.stream().map(service::toVo).toList();
    }
}
