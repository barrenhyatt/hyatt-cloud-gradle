package cn.hyatt.mapper;

import cn.hyatt.entity.SystemRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统角色Mapper
 *
 * @author hyatt
 */
@Mapper
public interface SystemRoleMapper extends BaseMapper<SystemRole> {
}
