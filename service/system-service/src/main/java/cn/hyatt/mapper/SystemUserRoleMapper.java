package cn.hyatt.mapper;

import cn.hyatt.entity.SystemUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户角色Mapper
 *
 * @author hyatt
 */
@Mapper
public interface SystemUserRoleMapper   extends BaseMapper<SystemUserRole> {
}
