package cn.hyatt.mapper;

import cn.hyatt.entity.SystemDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统字典Mapper
 *
 * @author hyatt
 */
@Mapper
public interface SystemDictMapper extends BaseMapper<SystemDict> {
}
