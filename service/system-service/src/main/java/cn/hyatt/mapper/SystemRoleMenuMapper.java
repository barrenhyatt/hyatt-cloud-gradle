package cn.hyatt.mapper;

import cn.hyatt.entity.SystemRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统角色菜单Mapper
 *
 * @author hyatt
 */
@Mapper
public interface SystemRoleMenuMapper  extends BaseMapper<SystemRoleMenu> {
}
