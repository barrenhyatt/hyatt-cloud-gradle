package cn.hyatt.mapper;

import cn.hyatt.entity.SystemDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统部门Mapper
 *
 * @author hyatt
 */
@Mapper
public interface SystemDeptMapper extends BaseMapper<SystemDept> {
}
