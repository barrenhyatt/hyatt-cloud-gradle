package cn.hyatt.mapper;

import cn.hyatt.entity.SystemConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 系统配置Mapper
 *
 * @author hyatt
 */
@Mapper
public interface SystemConfigMapper extends BaseMapper<SystemConfig> {
}
