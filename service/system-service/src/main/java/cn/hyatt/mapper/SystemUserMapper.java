package cn.hyatt.mapper;

import cn.hyatt.entity.SystemUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户Mapper
 *
 * @author hyatt
 */
@Mapper
public interface SystemUserMapper extends BaseMapper<SystemUser> {
}
