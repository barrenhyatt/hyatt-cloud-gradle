package cn.hyatt.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@EnableFeignClients
@MapperScan(value = "cn.hyatt.mapper")
@Configuration
public class SystemAppConfig {
}
