package cn.hyatt.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * 系统角色
 *
 * @author hyatt
 */
@Getter
@Setter
@TableName(value = "system_role")
@Schema(name = "SystemRole", title = "系统角色", description = "系统角色对象")
public class SystemRole extends BaseEntity {
    @Serial
    private static final long serialVersionUID = 42L;

    @NotNull
    @TableField(value = "name")
    @Schema(name = "name", title = "角色名称", description = "角色名称")
    private String name;

    @NotNull
    @TableField(value = "code")
    @Schema(name = "code", title = "角色代码", description = "角色代码")
    private String code;

    @TableField(value = "description")
    @Schema(name = "description", title = "角色描述", description = "角色描述")
    private String description;

}
