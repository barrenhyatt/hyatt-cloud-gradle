package cn.hyatt.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * 系统部门
 *
 * @author hyatt
 */
@Getter
@Setter
@TableName(value = "system_dept")
@Schema(name = "SystemDept", title = "系统部门", description = "系统部门对象")
public class SystemDept extends BaseEntity {
    @Serial
    private static final long serialVersionUID = 42L;

    @TableField(value = "parent_id")
    @Schema(name = "parentId", title = "父部门Id", description = "归属上级部门的Id")
    private Long parentId;

    @TableField(value = "relation_chain")
    @Schema(name = "relationChain",title = "部门关系链",description = "归属上级部门的关系链")
    private String relationChain;

    @NotNull
    @TableField(value = "name")
    @Schema(name = "name", title = "部门名称", description = "部门名称")
    private String name;

    @NotNull
    @TableField(value = "code")
    @Schema(name = "code", title = "部门代码", description = "部门代码")
    private String code;

    @TableField(value = "manager")
    @Schema(name = "manager", title = "管理者/负责人", description = "部门管理者")
    private String manager;

    @TableField(value = "phone")
    @Schema(name = "phone", title = "管理者/负责人电话", description = "部门管理者的电话")
    private String phone;

    @NotNull
    @TableField(value = "sort")
    @Schema(name = "sort", title = "排序", description = "排序:从小到大")
    private Integer sort;

    @NotNull
    @TableField(value = "enabled")
    @Schema(name = "enabled", title = "是否启用", description = "是否启用部门")
    private Boolean enabled;

}
