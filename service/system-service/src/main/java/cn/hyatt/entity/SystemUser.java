package cn.hyatt.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * 系统用户
 *
 * @author hyatt
 */
@Getter
@Setter
@TableName(value = "system_user")
@Schema(name = "SystemUser", title = "系统用户", description = "系统用户对象")
public class SystemUser extends BaseEntity {
    @Serial
    private static final long serialVersionUID = 42L;

    @NotNull
    @TableField(value = "username")
    @Schema(name = "username", title = "用户名", description = "用户名")
    private String username;

    @NotNull
    @TableField(value = "password")
    @Schema(name = "password", title = "用户密码", description = "用户密码")
    private String password;

    @NotNull
    @TableField(value = "dept_id")
    @Schema(name = "deptId", title = "部门Id", description = "用户所属部门Id")
    private String deptId;

    @NotNull
    @TableField(value = "nickname")
    @Schema(name = "nickname", title = "用户昵称", description = "用户昵称")
    private String nickname;

    @TableField(value = "phone")
    @Schema(name = "phone", title = "用户电话", description = "用户电话")
    private String phone;

    @TableField(value = "enabled")
    @Schema(name = "enabled", title = "是否启用", description = "是否启用")
    private String enabled;

}
