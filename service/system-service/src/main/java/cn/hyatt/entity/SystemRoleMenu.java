package cn.hyatt.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * 系统角色菜单
 *
 * @author hyatt
 */
@Getter
@Setter
@TableName(value = "system_role_menu")
@Schema(name = "SystemRoleMenu", title = "系统角色菜单", description = "系统角色关联菜单")
public class SystemRoleMenu extends BaseEntity {
    @Serial
    private static final long serialVersionUID = 42L;

    @NotNull
    @TableField(value = "role_id")
    @Schema(name = "roleId", title = "角色Id", description = "角色Id")
    private Long roleId;

    @NotNull
    @TableField(value = "menu_id")
    @Schema(name = "menuId", title = "菜单Id", description = "菜单Id")
    private Long menuId;

}
