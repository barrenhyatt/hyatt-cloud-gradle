package cn.hyatt.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;


/**
 * 系统部门
 *
 * @author hyatt
 */
@Getter
@Setter
@TableName(value = "system_config")
@Schema(name = "SystemAppConfig", title = "系统配置", description = "系统配置对象")
public class SystemConfig extends BaseEntity {
    @Serial
    private static final long serialVersionUID = 42L;

    @NotNull
    @TableField(value = "name")
    @Schema(name = "name", title = "配置名称", description = "系统配置名称")
    private String name;

    @NotNull
    @TableField(value = "code")
    @Schema(name = "code", title = "配置编码", description = "系统配置编码")
    private String code;

    @NotNull
    @TableField(value = "value")
    @Schema(name = "value", title = "配置值", description = "系统配置值")
    private String value;

    @TableField(value = "description")
    @Schema(name = "description", title = "配置描述", description = "配置描述")
    private String description;

}
