package cn.hyatt.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * 系统用户角色
 *
 * @author hyatt
 */
@Getter
@Setter
@TableName(value = "system_user_role")
@Schema(name = "SystemUserRole", title = "系统用户角色", description = "系统用户关联角色")
public class SystemUserRole extends IdEntity {
    @Serial
    private static final long serialVersionUID = 42L;

    @NotNull
    @TableField(value = "user_id")
    @Schema(name = "userId", title = "用户Id", description = "用户Id")
    private Long userId;

    @NotNull
    @TableField(value = "role_id")
    @Schema(name = "roleId", title = "角色Id", description = "角色Id")
    private Long roleId;
}
