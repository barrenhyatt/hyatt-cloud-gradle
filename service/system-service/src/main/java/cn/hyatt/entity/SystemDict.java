package cn.hyatt.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * 系统部门
 *
 * @author hyatt
 */
@Getter
@Setter
@TableName(value = "system_dict")
@Schema(name = "SystemDict", title = "系统字典", description = "系统字典对象")
public class SystemDict extends BaseEntity {
    @Serial
    private static final long serialVersionUID = 42L;

    @TableField(value = "parent_id")
    @Schema(name = "parentId", title = "字典类型", description = "归属上级字典类型的Id")
    private Long parentId;

    @NotNull
    @TableField(value = "name")
    @Schema(name = "name", title = "字典名称", description = "系统字典名称")
    private String name;

    @NotNull
    @TableField(value = "code")
    @Schema(name = "code", title = "字典编码", description = "系统字典编码")
    private String code;

    @TableField(value = "title")
    @Schema(name = "title", title = "字典项标题", description = "字典项标题")
    private String title;

    @TableField(value = "key")
    @Schema(name = "key", title = "字典项Key", description = "系统字典项的Key")
    private String key;

    @TableField(value = "value")
    @Schema(name = "value", title = "字典项值", description = "系统字典项的值")
    private String value;

    @TableField(value = "default_option")
    @Schema(name = "defaultOption", title = "字典项默认值", description = "系统字典项默认值,同一字典类只有一个默认值")
    private Boolean defaultOption;

    @TableField(value = "description")
    @Schema(name = "description", title = "字典描述", description = "字典描述")
    private String description;

    @AssertTrue(message = "字典项Key不能为空")
    public boolean titleNotNull() {
        if (parentId != null) {
            return title == null;
        }
        return false;
    }

    @AssertTrue(message = "字典项Key不能为空")
    public boolean keyNotNull() {
        if (parentId != null) {
            return key == null;
        }
        return false;
    }

    @AssertTrue(message = "字典项值不能为空")
    public boolean valueNotNull() {
        if (parentId != null) {
            return value == null;
        }
        return false;
    }
}
