package cn.hyatt.entity;

import cn.hyatt.enums.MenuType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.AssertFalse;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统菜单
 *
 * @author hyatt
 */
@Getter
@Setter
@TableName(value = "system_menu")
@Schema(name = "SystemMenu", title = "系统菜单", description = "系统菜单对象")
public class SystemMenu extends BaseEntity {
    @Serial
    private static final long serialVersionUID = 42L;

    @TableField(value = "parent_id")
    @Schema(name = "parentId", title = "父菜单Id", description = "归属上级父菜的Id")
    private Long parentId;

    @NotNull
    @TableField(value = "type")
    @Schema(name = "type", title = "菜单类型", description = "菜单类型")
    private MenuType type;

    @NotBlank
    @TableField(value = "title")
    @Schema(name = "title", title = "菜单标题", description = "菜单标题")
    private String title;

    @TableField(value = "icon")
    @Schema(name = "icon", title = "菜单图标", description = "菜单图标")
    private String icon;

    @NotNull
    @TableField(value = "visible")
    @Schema(name = "visible", title = "是否显示", description = "是否显示：true显示 false隐藏")
    private Boolean visible;

    @NotNull
    @TableField(value = "sort")
    @Schema(name = "sort", title = "排序", description = "排序")
    private Integer sort;

    @TableField(value = "path")
    @Schema(name = "path", title = "路由地址", description = "路由地址")
    private String path;

    @TableField(value = "name")
    @Schema(name = "name", title = "路由名称", description = "路由名称")
    private String name;

    @TableField(value = "component")
    @Schema(name = "component", title = "组件路径", description = "组件路径")
    private String component;

    @TableField(value = "link")
    @Schema(name = "link", title = "是否为外链", description = "是否为外链：true是 false否")
    private Boolean link;

    @TableField(value = "redirect")
    @Schema(name = "redirect", title = "重定向", description = "重定向")
    private String redirect;

    @TableField(value = "permissions")
    @Schema(name = "permissions", title = "权限字符串", description = "权限字符串")
    private String permissions;

    @NotNull
    @TableField(value = "enabled")
    @Schema(name = "enabled", title = "是否启用", description = "是否启用：true启用 false未启用")
    private Boolean enabled;


    @TableField(exist = false)
    @Schema(name = "children", title = "子菜单", description = "子菜单")
    private List<SystemMenu> children = new ArrayList<>();

    @AssertFalse(message = "路由地址不能为空")
    public boolean isCanPathBeNull() {
        if (type.equals(MenuType.MENU)) {
            return path == null;
        }
        return false;
    }

    @AssertFalse(message = "路由名称不能为空")
    public boolean isCanNameBeNull() {
        if (type.equals(MenuType.MENU)) {
            return name == null;
        }
        return false;
    }

    @AssertFalse(message = "组件路径不能为空")
    public boolean isCanComponentBeNull() {
        if (type.equals(MenuType.MENU) || type.equals(MenuType.DIRECTORY)) {
            return component == null;
        }
        return false;
    }

    @AssertFalse(message = "链接不能为空")
    public boolean isCanLinkBeNull() {
        if (type.equals(MenuType.MENU)) {
            return link == null;
        }
        return false;
    }

    @AssertFalse(message = "重定向不能为空")
    public boolean isCanRedirectBeNull() {
        if (type.equals(MenuType.MENU) && link) {
            return redirect == null;
        }
        return false;
    }

    @AssertFalse(message = "权限字符串不能为空")
    public boolean isCanPermissionsBeNull() {
        if (type.equals(MenuType.BUTTON)) {
            return permissions == null;
        }
        return false;
    }

}
