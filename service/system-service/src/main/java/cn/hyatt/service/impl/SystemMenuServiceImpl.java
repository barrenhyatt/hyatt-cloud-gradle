package cn.hyatt.service.impl;

import cn.hyatt.dto.systemMenu.SystemMenuFindDto;
import cn.hyatt.dto.systemMenu.SystemMenuSaveDto;
import cn.hyatt.dto.systemMenu.SystemMenuUpdateDto;
import cn.hyatt.dto.systemMenu.SystemMenuVo;
import cn.hyatt.entity.SystemMenu;
import cn.hyatt.mapper.SystemMenuMapper;
import cn.hyatt.service.SystemMenuService;
import cn.hyatt.utils.BeanCopyUtils;
import cn.hyatt.utils.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * 系统部门服务
 *
 * @author hyatt
 */
@Slf4j
@Service
@AllArgsConstructor
public class SystemMenuServiceImpl extends ServiceImpl<SystemMenuMapper, SystemMenu> implements SystemMenuService {
    @Override
    public List<SystemMenu> findByList(SystemMenuFindDto dto) {
        return baseMapper.selectList(queryWrapper(dto));
    }

    @Override
    public Page<SystemMenu> findByPage(SystemMenuFindDto dto) {
        return baseMapper.selectPage(PageUtil.page(dto), queryWrapper(dto));
    }

    @Override
    @Transactional
    public SystemMenu save(SystemMenuSaveDto dto) {
        SystemMenu entity = new SystemMenu();
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional
    public void update(Long id, SystemMenuUpdateDto dto) {
        Optional<SystemMenu> optional = getOptById(id);
        SystemMenu entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.updateById(entity);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        baseMapper.deleteById(id);
    }

    @Override
    public SystemMenuVo toVo(SystemMenu entity) {
        SystemMenuVo vo = new SystemMenuVo();
        BeanCopyUtils.copyProperties(vo, entity);
        return vo;
    }

    private LambdaQueryWrapper<SystemMenu> queryWrapper(SystemMenuFindDto dto) {
        return new LambdaQueryWrapper<SystemMenu>()
                .eq(dto.getCreateTime() != null, SystemMenu::getCreateTime, dto.getCreateTime())
                .eq(dto.getUpdateTime() != null, SystemMenu::getUpdateTime, dto.getUpdateTime())
                .eq(dto.getParentId() != null, SystemMenu::getParentId, dto.getParentId())
                .eq(dto.getType() != null, SystemMenu::getType, dto.getType())
                .eq(dto.getTitle() != null, SystemMenu::getTitle, dto.getTitle())
                .eq(dto.getIcon() != null, SystemMenu::getIcon, dto.getIcon())
                .eq(dto.getVisible() != null, SystemMenu::getVisible, dto.getVisible())
                .eq(dto.getSort() != null, SystemMenu::getSort, dto.getSort())
                .eq(dto.getPath() != null, SystemMenu::getPath, dto.getPath())
                .eq(dto.getName() != null, SystemMenu::getName, dto.getName())
                .eq(dto.getComponent() != null, SystemMenu::getComponent, dto.getComponent())
                .eq(dto.getLink() != null, SystemMenu::getLink, dto.getLink())
                .eq(dto.getRedirect() != null, SystemMenu::getRedirect, dto.getRedirect())
                .eq(dto.getPermissions() != null, SystemMenu::getPermissions, dto.getPermissions())
                .eq(dto.getEnabled() != null, SystemMenu::getEnabled, dto.getEnabled());
    }
}
