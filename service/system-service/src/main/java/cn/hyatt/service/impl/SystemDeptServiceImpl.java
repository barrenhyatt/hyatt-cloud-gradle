package cn.hyatt.service.impl;

import cn.hyatt.dto.systemDept.SystemDeptFindDto;
import cn.hyatt.dto.systemDept.SystemDeptSaveDto;
import cn.hyatt.dto.systemDept.SystemDeptUpdateDto;
import cn.hyatt.dto.systemDept.SystemDeptVo;
import cn.hyatt.entity.SystemDept;
import cn.hyatt.mapper.SystemDeptMapper;
import cn.hyatt.service.SystemDeptService;
import cn.hyatt.utils.BeanCopyUtils;
import cn.hyatt.utils.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 系统部门服务
 *
 * @author hyatt
 */
@Slf4j
@Service
@AllArgsConstructor
public class SystemDeptServiceImpl extends ServiceImpl<SystemDeptMapper, SystemDept> implements SystemDeptService {
    @Override
    public List<SystemDept> findByList(SystemDeptFindDto dto) {
        return baseMapper.selectList(queryWrapper(dto));
    }

    @Override
    public IPage<SystemDept> findByPage(SystemDeptFindDto dto) {
        return baseMapper.selectPage(PageUtil.page(dto), queryWrapper(dto));
    }

    @Override
    @Transactional
    public SystemDept save(SystemDeptSaveDto dto) {
        SystemDept entity = new SystemDept();
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional
    public void update(Long id, SystemDeptUpdateDto dto) {
        Optional<SystemDept> optional = getOptById(id);
        SystemDept entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.updateById(entity);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        baseMapper.deleteById(id);
    }

    @Override
    public SystemDeptVo toVo(SystemDept entity) {
        SystemDeptVo vo = new SystemDeptVo();
        BeanCopyUtils.copyProperties(vo, entity);
        return vo;
    }

    private LambdaQueryWrapper<SystemDept> queryWrapper(SystemDeptFindDto dto) {
        return new LambdaQueryWrapper<SystemDept>()
                .eq(dto.getParentId() != null, SystemDept::getParentId, dto.getParentId())
                .like(dto.getRelationChain() != null, SystemDept::getRelationChain, dto.getRelationChain())
                .like(dto.getName() != null, SystemDept::getName, dto.getName())
                .like(dto.getCode() != null, SystemDept::getCode, dto.getCode())
                .like(dto.getManager() != null, SystemDept::getManager, dto.getManager())
                .like(dto.getPhone() != null, SystemDept::getPhone, dto.getPhone())
                .eq(dto.getCreateTime() != null, SystemDept::getCreateTime, dto.getCreateTime())
                .eq(dto.getUpdateTime() != null, SystemDept::getUpdateTime, dto.getUpdateTime());
    }
}
