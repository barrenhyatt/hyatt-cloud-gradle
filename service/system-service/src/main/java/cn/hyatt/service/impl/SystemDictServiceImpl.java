package cn.hyatt.service.impl;

import cn.hyatt.dto.systemDict.SystemDictFindDto;
import cn.hyatt.dto.systemDict.SystemDictSaveDto;
import cn.hyatt.dto.systemDict.SystemDictUpdateDto;
import cn.hyatt.dto.systemDict.SystemDictVo;
import cn.hyatt.entity.SystemDict;
import cn.hyatt.mapper.SystemDictMapper;
import cn.hyatt.service.SystemDictService;
import cn.hyatt.utils.BeanCopyUtils;
import cn.hyatt.utils.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class SystemDictServiceImpl extends ServiceImpl<SystemDictMapper, SystemDict> implements SystemDictService {
    @Override
    public List<SystemDict> findByList(SystemDictFindDto dto) {
        return baseMapper.selectList(queryWrapper(dto));
    }

    @Override
    public Page<SystemDict> findByPage(SystemDictFindDto dto) {
        return baseMapper.selectPage(PageUtil.page(dto), queryWrapper(dto));
    }

    @Override
    @Transactional
    public SystemDict save(SystemDictSaveDto dto) {
        SystemDict entity = new SystemDict();
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional
    public void update(Long id, SystemDictUpdateDto dto) {
        Optional<SystemDict> optional = getOptById(id);
        SystemDict entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.updateById(entity);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        baseMapper.deleteById(id);
    }

    @Override
    public SystemDictVo toVo(SystemDict entity) {
        SystemDictVo vo = new SystemDictVo();
        BeanCopyUtils.copyProperties(vo, entity);
        return vo;
    }

    private LambdaQueryWrapper<SystemDict> queryWrapper(SystemDictFindDto dto) {
        return new LambdaQueryWrapper<SystemDict>()
                .eq(dto.getParentId() != null, SystemDict::getParentId, dto.getParentId())
                .eq(dto.getName() != null, SystemDict::getName, dto.getName())
                .eq(dto.getCode() != null, SystemDict::getCode, dto.getCode());
    }
}
