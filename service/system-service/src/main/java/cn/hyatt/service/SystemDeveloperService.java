package cn.hyatt.service;

/**
 * 系统开发者服务接口
 *
 * @author hyatt
 */
public interface SystemDeveloperService {
    /**
     * 模块初始化
     */
    void init();
}
