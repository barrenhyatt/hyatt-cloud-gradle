package cn.hyatt.service;

import cn.hyatt.dto.systemDict.SystemDictFindDto;
import cn.hyatt.dto.systemDict.SystemDictSaveDto;
import cn.hyatt.dto.systemDict.SystemDictUpdateDto;
import cn.hyatt.dto.systemDict.SystemDictVo;
import cn.hyatt.entity.SystemDict;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * 系统字典服务接口
 *
 * @author hyatt
 */
public interface SystemDictService extends IService<SystemDict> {

    /**
     * 按列表查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    List<SystemDict> findByList(SystemDictFindDto dto);

    /**
     * 按分页查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    IPage<SystemDict> findByPage(SystemDictFindDto dto);

    /**
     * 新增保存
     *
     * @param dto 保存对象
     */
    SystemDict save(SystemDictSaveDto dto);

    /**
     * 更新
     *
     * @param id Id
     * @param dto 更新对象
     */
    void update(Long id, SystemDictUpdateDto dto);

    /**
     * 删除
     *
     * @param id Id
     */
    void delete(Long id);

    /**
     * 转VO
     *
     * @param entity 实体
     */
    SystemDictVo toVo(SystemDict entity);
}
