package cn.hyatt.service.impl;

import cn.hyatt.dto.systemUser.SystemUserFindDto;
import cn.hyatt.dto.systemUser.SystemUserSaveDto;
import cn.hyatt.dto.systemUser.SystemUserUpdateDto;
import cn.hyatt.dto.systemUser.SystemUserVo;
import cn.hyatt.entity.SystemUser;
import cn.hyatt.mapper.SystemUserMapper;
import cn.hyatt.service.SystemUserRoleService;
import cn.hyatt.service.SystemUserService;
import cn.hyatt.utils.BeanCopyUtils;
import cn.hyatt.utils.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 系统部门服务
 *
 * @author hyatt
 */
@Slf4j
@Service
@AllArgsConstructor
public class SystemUserServiceImpl extends ServiceImpl<SystemUserMapper, SystemUser> implements SystemUserService {

   private SystemUserRoleService systemUserRoleService;

    @Override
    public List<SystemUser> findByList(SystemUserFindDto dto) {
        return baseMapper.selectList(queryWrapper(dto));
    }

    @Override
    public IPage<SystemUser> findByPage(SystemUserFindDto dto) {
        return baseMapper.selectPage(PageUtil.page(dto), queryWrapper(dto));
    }

    @Override
    @Transactional
    public SystemUser save(SystemUserSaveDto dto) {
        SystemUser entity = new SystemUser();
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.insert(entity);
        // 保存用户关联角色
        systemUserRoleService.batchSaveByRoleId(entity.getId(),dto.getRoleList());
        return entity;
    }

    @Override
    @Transactional
    public void update(Long id, SystemUserUpdateDto dto) {
        Optional<SystemUser> optional = getOptById(id);
        SystemUser entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.updateById(entity);
        // 更新用户关联角色
        if (dto.getRoleList() != null && !dto.getRoleList().isEmpty()) {
            systemUserRoleService.deleteByUserId(id);
            systemUserRoleService.batchSaveByRoleId(id,dto.getRoleList());
        }
    }

    @Override
    @Transactional
    public void delete(Long id) {
        baseMapper.deleteById(id);
        // 删除角色关联菜单
        systemUserRoleService.deleteByUserId(id);
    }

    @Override
    public SystemUserVo toVo(SystemUser entity) {
        SystemUserVo vo = new SystemUserVo();
        BeanCopyUtils.copyProperties(vo, entity);
        return vo;
    }

    private LambdaQueryWrapper<SystemUser> queryWrapper(SystemUserFindDto dto) {
        return new LambdaQueryWrapper<SystemUser>()
                .eq(dto.getCreateTime() != null, SystemUser::getCreateTime, dto.getCreateTime())
                .eq(dto.getUpdateTime() != null, SystemUser::getUpdateTime, dto.getUpdateTime())
                .eq(dto.getUsername() != null, SystemUser::getUsername, dto.getUsername())
                .eq(dto.getDeptId() != null, SystemUser::getDeptId, dto.getDeptId())
                .eq(dto.getNickname() != null, SystemUser::getNickname, dto.getNickname())
                .eq(dto.getPhone() != null, SystemUser::getPhone, dto.getPhone());
    }
}
