package cn.hyatt.service;

import cn.hyatt.dto.systemMenu.SystemMenuFindDto;
import cn.hyatt.dto.systemMenu.SystemMenuSaveDto;
import cn.hyatt.dto.systemMenu.SystemMenuUpdateDto;
import cn.hyatt.dto.systemMenu.SystemMenuVo;
import cn.hyatt.entity.SystemMenu;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 系统部门 服务接口
 *
 * @author hyatt
 */
public interface SystemMenuService extends IService<SystemMenu> {

    /**
     * 按列表查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    List<SystemMenu> findByList(SystemMenuFindDto dto);

    /**
     * 按分页查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    IPage<SystemMenu> findByPage(SystemMenuFindDto dto);

    /**
     * 新增保存
     *
     * @param dto 保存对象
     */
    SystemMenu save(SystemMenuSaveDto dto);

    /**
     * 更新
     *
     * @param id Id
     * @param dto 更新对象
     */
    void update(Long id, SystemMenuUpdateDto dto);

    /**
     * 删除
     *
     * @param id Id
     */
    void delete(Long id);

    /**
     * 转VO
     *
     * @param entity 实体
     */
    SystemMenuVo toVo(SystemMenu entity);
}
