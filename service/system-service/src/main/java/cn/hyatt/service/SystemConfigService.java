package cn.hyatt.service;

import cn.hyatt.dto.systemConfig.SystemConfigFindDto;
import cn.hyatt.dto.systemConfig.SystemConfigSaveDto;
import cn.hyatt.dto.systemConfig.SystemConfigUpdateDto;
import cn.hyatt.dto.systemConfig.SystemConfigVo;
import cn.hyatt.entity.SystemConfig;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * 系统配置服务接口
 *
 * @author hyatt
 */
public interface SystemConfigService extends IService<SystemConfig> {

    /**
     * 按列表查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    List<SystemConfig> findByList(SystemConfigFindDto dto);

    /**
     * 按分页查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    IPage<SystemConfig> findByPage(SystemConfigFindDto dto);

    /**
     * 新增保存
     *
     * @param dto 保存对象
     */
    SystemConfig save(SystemConfigSaveDto dto);

    /**
     * 更新
     *
     * @param id Id
     * @param dto 更新对象
     */
    void update(Long id, SystemConfigUpdateDto dto);

    /**
     * 删除
     *
     * @param id Id
     */
    void delete(Long id);

    /**
     * 转VO
     *
     * @param entity 实体
     */
    SystemConfigVo toVo(SystemConfig entity);
}
