package cn.hyatt.service.impl;

import cn.hyatt.entity.SystemUserRole;
import cn.hyatt.mapper.SystemUserRoleMapper;
import cn.hyatt.service.SystemUserRoleService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 系统用户角色 服务
 *
 * @author hyatt
 */
@Slf4j
@Service
@AllArgsConstructor
public class SystemUserRoleServiceImpl extends ServiceImpl<SystemUserRoleMapper, SystemUserRole> implements SystemUserRoleService {
    @Override
    public List<SystemUserRole> findByUserId(Long userId) {
        return baseMapper.selectList(new LambdaQueryWrapper<SystemUserRole>()
                .eq(SystemUserRole::getUserId, userId));
    }

    @Override
    @Transactional
    public void batchSaveByRoleId(Long userId, List<Long> menuIds) {
        List<SystemUserRole> list = menuIds.stream().map(roleId -> {
            SystemUserRole systemRoleMenu = new SystemUserRole();
            systemRoleMenu.setUserId(userId);
            systemRoleMenu.setRoleId(roleId);
            return systemRoleMenu;
        }).toList();
        saveBatch(list);
    }

    @Override
    @Transactional
    public void deleteByUserId(Long userId) {
        baseMapper.delete(new LambdaQueryWrapper<SystemUserRole>()
                .eq(SystemUserRole::getUserId, userId));
    }
}
