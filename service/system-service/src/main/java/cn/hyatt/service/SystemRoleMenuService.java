package cn.hyatt.service;

import cn.hyatt.entity.SystemRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 系统角色菜单 服务接口
 *
 * @author hyatt
 */
public interface SystemRoleMenuService extends IService<SystemRoleMenu> {

    /**
     * 按 角色Id 查询
     * @param roleId 角色Id
     * @return 结果
     */
    List<SystemRoleMenu> findByRoleId(Long roleId);

    /**
     * 按 角色Id 批量保存
     * @param roleId 角色Id
     * @param menuIds 菜单Ids
     */
    void batchSaveByRoleId(Long roleId, List<Long> menuIds);

    /**
     * 按 角色Id 删除
     * @param roleId 角色Id
     */
    void deleteByRoleId(Long roleId);
}
