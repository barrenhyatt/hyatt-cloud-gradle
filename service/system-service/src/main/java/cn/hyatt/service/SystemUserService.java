package cn.hyatt.service;

import cn.hyatt.dto.systemUser.SystemUserFindDto;
import cn.hyatt.dto.systemUser.SystemUserSaveDto;
import cn.hyatt.dto.systemUser.SystemUserUpdateDto;
import cn.hyatt.dto.systemUser.SystemUserVo;
import cn.hyatt.entity.SystemUser;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 系统用户 服务接口
 *
 * @author hyatt
 */
public interface SystemUserService extends IService<SystemUser> {

    /**
     * 按列表查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    List<SystemUser> findByList(SystemUserFindDto dto);

    /**
     * 按分页查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    IPage<SystemUser> findByPage(SystemUserFindDto dto);

    /**
     * 新增保存
     *
     * @param dto 保存对象
     */
    SystemUser save(SystemUserSaveDto dto);

    /**
     * 更新
     *
     * @param id Id
     * @param dto 更新对象
     */
    void update(Long id, SystemUserUpdateDto dto);

    /**
     * 删除
     *
     * @param id Id
     */
    void delete(Long id);

    /**
     * 转VO
     *
     * @param entity 实体
     */
    SystemUserVo toVo(SystemUser entity);
}
