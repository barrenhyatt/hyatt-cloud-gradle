package cn.hyatt.service;

import cn.hyatt.dto.systemRole.SystemRoleFindDto;
import cn.hyatt.dto.systemRole.SystemRoleSaveDto;
import cn.hyatt.dto.systemRole.SystemRoleUpdateDto;
import cn.hyatt.dto.systemRole.SystemRoleVo;
import cn.hyatt.entity.SystemRole;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 系统用户 服务接口
 *
 * @author hyatt
 */
public interface SystemRoleService extends IService<SystemRole> {

    /**
     * 按列表查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    List<SystemRole> findByList(SystemRoleFindDto dto);

    /**
     * 按分页查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    IPage<SystemRole> findByPage(SystemRoleFindDto dto);

    /**
     * 新增保存
     *
     * @param dto 保存对象
     */
    SystemRole save(SystemRoleSaveDto dto);

    /**
     * 更新
     *
     * @param id Id
     * @param dto 更新对象
     */
    void update(Long id, SystemRoleUpdateDto dto);

    /**
     * 删除
     *
     * @param id Id
     */
    void delete(Long id);

    /**
     * 转VO
     *
     * @param entity 实体
     */
    SystemRoleVo toVo(SystemRole entity);
}
