package cn.hyatt.service.impl;

import cn.hyatt.dto.systemRole.SystemRoleFindDto;
import cn.hyatt.dto.systemRole.SystemRoleSaveDto;
import cn.hyatt.dto.systemRole.SystemRoleUpdateDto;
import cn.hyatt.dto.systemRole.SystemRoleVo;
import cn.hyatt.entity.SystemRole;
import cn.hyatt.mapper.SystemRoleMapper;
import cn.hyatt.service.SystemRoleMenuService;
import cn.hyatt.service.SystemRoleService;
import cn.hyatt.utils.BeanCopyUtils;
import cn.hyatt.utils.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 系统部门服务
 *
 * @author hyatt
 */
@Slf4j
@Service
@AllArgsConstructor
public class SystemRoleServiceImpl extends ServiceImpl<SystemRoleMapper, SystemRole> implements SystemRoleService {

    private SystemRoleMenuService systemRoleMenuService;

    @Override
    public List<SystemRole> findByList(SystemRoleFindDto dto) {
        return baseMapper.selectList(queryWrapper(dto));
    }

    @Override
    public IPage<SystemRole> findByPage(SystemRoleFindDto dto) {
        return baseMapper.selectPage(PageUtil.page(dto), queryWrapper(dto));
    }

    @Override
    @Transactional
    public SystemRole save(SystemRoleSaveDto dto) {
        SystemRole entity = new SystemRole();
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.insert(entity);
        // 保存角色关联菜单
        systemRoleMenuService.batchSaveByRoleId(entity.getId(),dto.getMenuList());
        return entity;
    }

    @Override
    @Transactional
    public void update(Long id, SystemRoleUpdateDto dto) {
        Optional<SystemRole> optional = getOptById(id);
        SystemRole entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.updateById(entity);
        // 更新角色关联菜单
        if (dto.getMenuList() != null && !dto.getMenuList().isEmpty()) {
            systemRoleMenuService.deleteByRoleId(id);
            systemRoleMenuService.batchSaveByRoleId(id,dto.getMenuList());
        }
    }

    @Override
    @Transactional
    public void delete(Long id) {
        baseMapper.deleteById(id);
        // 删除角色关联菜单
        systemRoleMenuService.deleteByRoleId(id);
    }

    @Override
    public SystemRoleVo toVo(SystemRole entity) {
        SystemRoleVo vo = new SystemRoleVo();
        BeanCopyUtils.copyProperties(vo, entity);
        return vo;
    }

    private LambdaQueryWrapper<SystemRole> queryWrapper(SystemRoleFindDto dto) {
        return new LambdaQueryWrapper<SystemRole>()
                .eq(dto.getCreateTime() != null, SystemRole::getCreateTime, dto.getCreateTime())
                .eq(dto.getUpdateTime() != null, SystemRole::getUpdateTime, dto.getUpdateTime())
                .eq(dto.getName() != null, SystemRole::getName, dto.getName())
                .eq(dto.getCode() != null, SystemRole::getCode, dto.getCode())
                .eq(dto.getDescription() != null, SystemRole::getDescription, dto.getDescription());
    }
}
