package cn.hyatt.service.impl;

import cn.hyatt.dto.systemConfig.SystemConfigFindDto;
import cn.hyatt.dto.systemConfig.SystemConfigSaveDto;
import cn.hyatt.dto.systemConfig.SystemConfigUpdateDto;
import cn.hyatt.dto.systemConfig.SystemConfigVo;
import cn.hyatt.entity.SystemConfig;
import cn.hyatt.mapper.SystemConfigMapper;
import cn.hyatt.service.SystemConfigService;
import cn.hyatt.utils.BeanCopyUtils;
import cn.hyatt.utils.PageUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 系统配置服务
 *
 * @author hyatt
 */

@Slf4j
@Service
@AllArgsConstructor
public class SystemConfigServiceImpl extends ServiceImpl<SystemConfigMapper, SystemConfig> implements SystemConfigService {
    @Override
    public List<SystemConfig> findByList(SystemConfigFindDto dto) {
        return baseMapper.selectList(queryWrapper(dto));
    }

    @Override
    public IPage<SystemConfig> findByPage(SystemConfigFindDto dto) {
        return baseMapper.selectPage(PageUtil.page(dto), queryWrapper(dto));
    }

    @Override
    @Transactional
    public SystemConfig save(SystemConfigSaveDto dto) {
        SystemConfig entity = new SystemConfig();
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional
    public void update(Long id, SystemConfigUpdateDto dto) {
        Optional<SystemConfig> optional = getOptById(id);
        SystemConfig entity = optional.orElseThrow(() -> new RuntimeException("找不到数据"));
        BeanCopyUtils.copyProperties(entity, dto);
        baseMapper.updateById(entity);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        baseMapper.deleteById(id);
    }

    @Override
    public SystemConfigVo toVo(SystemConfig entity) {
        SystemConfigVo vo = new SystemConfigVo();
        BeanCopyUtils.copyProperties(vo, entity);
        return vo;
    }

    private LambdaQueryWrapper<SystemConfig> queryWrapper(SystemConfigFindDto dto) {
        return new LambdaQueryWrapper<SystemConfig>()
                .eq(dto.getName() != null, SystemConfig::getName, dto.getName())
                .like(dto.getCode() != null, SystemConfig::getCode, dto.getCode());
    }
}

