package cn.hyatt.service.impl;

import cn.hyatt.entity.SystemRoleMenu;
import cn.hyatt.mapper.SystemRoleMenuMapper;
import cn.hyatt.service.SystemRoleMenuService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 系统角色菜单 服务
 *
 * @author hyatt
 */
@Slf4j
@Service
@AllArgsConstructor
public class SystemRoleMenuServiceImpl extends ServiceImpl<SystemRoleMenuMapper, SystemRoleMenu> implements SystemRoleMenuService {
    @Override
    public List<SystemRoleMenu> findByRoleId(Long roleId) {
        return baseMapper.selectList(new LambdaQueryWrapper<SystemRoleMenu>()
                .eq(SystemRoleMenu::getRoleId,roleId));
    }

    @Override
    @Transactional
    public void batchSaveByRoleId(Long roleId, List<Long> menuIds) {
        List<SystemRoleMenu> list = menuIds.stream().map(menuId -> {
            SystemRoleMenu systemRoleMenu = new SystemRoleMenu();
            systemRoleMenu.setRoleId(roleId);
            systemRoleMenu.setMenuId(menuId);
            return systemRoleMenu;
        }).toList();
        saveBatch(list);
    }

    @Override
    @Transactional
    public void deleteByRoleId(Long roleId) {
        baseMapper.delete(new LambdaQueryWrapper<SystemRoleMenu>()
                .eq(SystemRoleMenu::getRoleId, roleId));
    }
}
