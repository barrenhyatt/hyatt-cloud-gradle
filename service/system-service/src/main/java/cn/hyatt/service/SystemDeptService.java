package cn.hyatt.service;

import cn.hyatt.dto.systemDept.SystemDeptFindDto;
import cn.hyatt.dto.systemDept.SystemDeptSaveDto;
import cn.hyatt.dto.systemDept.SystemDeptUpdateDto;
import cn.hyatt.dto.systemDept.SystemDeptVo;
import cn.hyatt.entity.SystemDept;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 系统部门 服务接口
 *
 * @author hyatt
 */
public interface SystemDeptService extends IService<SystemDept> {

    /**
     * 按列表查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    List<SystemDept> findByList(SystemDeptFindDto dto);

    /**
     * 按分页查询
     *
     * @param dto 查询对象
     * @return 结果
     */
    IPage<SystemDept> findByPage(SystemDeptFindDto dto);

    /**
     * 新增保存
     *
     * @param dto 保存对象
     */
    SystemDept save(SystemDeptSaveDto dto);

    /**
     * 更新
     *
     * @param id Id
     * @param dto 更新对象
     */
    void update(Long id, SystemDeptUpdateDto dto);

    /**
     * 删除
     *
     * @param id Id
     */
    void delete(Long id);

    /**
     * 转VO
     *
     * @param entity 实体
     */
    SystemDeptVo toVo(SystemDept entity);
}
