package cn.hyatt.service;

import cn.hyatt.entity.SystemUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 系统用户角色 服务接口
 *
 * @author hyatt
 */
public interface SystemUserRoleService extends IService<SystemUserRole> {

    /**
     * 按 用户Id 查询
     * @param userId 用户Id
     * @return 结果
     */
    List<SystemUserRole> findByUserId(Long userId);

    /**
     * 按 用户Id 批量保存
     * @param userId 用户Id
     * @param roleIds 角色Ids
     */
    void batchSaveByRoleId(Long userId, List<Long> roleIds);

    /**
     * 按 用户Id 删除
     * @param userId 用户Id
     */
    void deleteByUserId(Long userId);
}
