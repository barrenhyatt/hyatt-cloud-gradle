﻿package ${package}.service.impl;

import ${package}.dto.${dtoPackageName}.${tableInfo.tableName}FindDto;
import ${package}.dto.${dtoPackageName}.${tableInfo.tableName}SaveDto;
import ${package}.dto.${dtoPackageName}.${tableInfo.tableName}UpdateDto;
import ${package}.dto.${dtoPackageName}.${tableInfo.tableName}Vo;
import ${package}.entity.${tableInfo.tableName};
import ${package}.event.${tableInfo.tableName}Event;
import ${package}.mapper.${tableInfo.tableName}Mapper;
import ${package}.service.${tableInfo.tableName}Service;
import cn.hyatt.common.exception.BusinessException;
import cn.hyatt.common.utils.BeanCopyUtil;
import cn.hyatt.common.utils.ValidationUtil;
import cn.hyatt.core.utils.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * ${tableInfo.tableRemark}-服务实现
 *
 * @author ${author}
 */
@Slf4j
@Service
@Primary
@AllArgsConstructor
public class ${tableInfo.tableName}ServiceImpl extends ServiceImpl<${tableInfo.tableName}Mapper, ${tableInfo.tableName}> implements ${tableInfo.tableName}Service {

    private ApplicationEventPublisher publisher;

    @Override
    public List<${tableInfo.tableName}> findByList(${tableInfo.tableName}FindDto dto) {
        return baseMapper.findByList(dto);
    }

    @Override
    public IPage<${tableInfo.tableName}> findByPage(${tableInfo.tableName}FindDto dto) {
        return baseMapper.findByPage(PageUtil.page(dto),dto);
    }

    @Override
    @Transactional
    public void save(${tableInfo.tableName}SaveDto dto) {
        // Copy 属性
        ${tableInfo.tableName} entity = new ${tableInfo.tableName}();
        BeanCopyUtil.copyProperties(dto, entity);
        // 验证数据内容
        ValidationUtil.validate(entity);
        // 插入数据
        baseMapper.insert(entity);

        // 发布事件
        publisher.publishEvent(new ${tableInfo.tableName}Event.Save(entity));
    }

    @Override
    @Transactional
    public void update(Long id, ${tableInfo.tableName}UpdateDto dto) {
        // 获取修改对象
        Optional<${tableInfo.tableName}> optional = getOptById(id);
        ${tableInfo.tableName} entity = optional.orElseThrow(
                () -> new BusinessException("找不到Id为(" + id + ")的数据")
        );
        // 保留旧实体记录
        ${tableInfo.tableName} oldEntity = new ${tableInfo.tableName}();
        BeanCopyUtil.copyProperties(entity, oldEntity);
        // Copy 属性
        BeanCopyUtil.copyProperties(dto, entity);
        // 验证数据内容
        ValidationUtil.validate(entity);
        // 按Id更新
        baseMapper.updateById(entity);

        // 发布事件
        publisher.publishEvent(new ${tableInfo.tableName}Event.Update(oldEntity, entity));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        // 按Id删除
        baseMapper.deleteById(id);
        // 发布事件
        publisher.publishEvent(new ${tableInfo.tableName}Event.Delete(id));
    }

    @Override
    public ${tableInfo.tableName}Vo toVo(${tableInfo.tableName} entity) {
        // Copy 属性
        ${tableInfo.tableName}Vo vo = new ${tableInfo.tableName}Vo();
        BeanCopyUtil.copyProperties(entity, vo);
        return vo;
    }
}
