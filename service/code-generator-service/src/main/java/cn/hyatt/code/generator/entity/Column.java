package cn.hyatt.code.generator.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@Schema(name = "数据库列信息")
public class Column implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;


    @TableField(value = "TABLE_CATALOG")
    @Schema(description = "表目录")
    private String tableCatalog;

    @TableField(value = "TABLE_SCHEMA")
    @Schema(description = "表所在数据库")
    private String tableSchema;

    @TableField(value = "TABLE_NAME")
    @Schema(description = "表名称")
    private String tableName;

    @TableField(value = "COLUMN_NAME")
    @Schema(description = "列名称")
    private String columnName;

    @TableField(value = "ORDINAL_POSITION")
    @Schema(description = "列顺序")
    private Integer ordinalPosition;


    @TableField(value = "COLUMN_DEFAULT")
    @Schema(description = "列默认值")
    private String columnDefault;

    @TableField(value = "IS_NULLABLE")
    @Schema(description = "是否可为空")
    private String isNullable;

    @TableField(value = "DATA_TYPE")
    @Schema(description = "数据类型")
    private String dataType;


    @TableField(value = "CHARACTER_MAXIMUM_LENGTH")
    @Schema(description = "字符最大长度")
    private Long characterMaximumLength;


    @TableField(value = "CHARACTER_OCTET_LENGTH")
    @Schema(description = "字符字节长度")
    private Long characterOctetLength;


    @TableField(value = "NUMERIC_PRECISION")
    @Schema(description = "数值精度")
    private Long numericPrecision;


    @TableField(value = "NUMERIC_SCALE")
    @Schema(description = "数字刻度")
    private Long numericScale;


    @TableField(value = "DATETIME_PRECISION")
    @Schema(description = "日期时间精度")
    private Integer datetimePrecision;


    @TableField(value = "CHARACTER_SET_NAME")
    @Schema(description = "字符集名称")
    private String characterSetName;


    @TableField(value = "COLLATION_NAME")
    @Schema(description = "排序规则名称")
    private String collationName;

    @TableField(value = "COLUMN_TYPE")
    @Schema(description = "列类型")
    private String columnType;


    @TableField(value = "COLUMN_KEY")
    @Schema(description = "列Key")
    private String columnKey;


    @TableField(value = "EXTRA")
    @Schema(description = "额外")
    private String extra;

    @TableField(value = "PRIVILEGES")
    @Schema(description = "特权")
    private String privileges;


    @TableField(value = "COLUMN_COMMENT")
    @Schema(description = "列注释")
    private String columnComment;


    @TableField(value = "GENERATION_EXPRESSION")
    @Schema(description = "generation 表达式")
    private String generationExpression;


    @TableField(value = "SRS_ID")
    @Schema(description = "SrsId")
    private Integer srsId;

}
