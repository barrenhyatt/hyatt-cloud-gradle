package cn.hyatt.code.generator.data;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ColumnItem {
    @NotBlank
    @Schema(description = "列名称-数据库列名")
    private String oldColumnName;

    @NotBlank
    @Schema(description = "字段名")
    private String columnName;

    @NotBlank
    @Schema(description = "字段备注")
    private String columnRemark;

    @NotBlank
    @Schema(description = "字段数据类型")
    private String columnDataType;

    @NotNull
    @Schema(description = "是否可为空")
    private Boolean isNullable;
}
