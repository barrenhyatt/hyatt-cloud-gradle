package cn.hyatt.code.generator.service;

import cn.hyatt.code.generator.dto.CodeGeneratorConfig;

import java.io.IOException;
import java.util.zip.ZipOutputStream;

public interface CodeGeneratorService {

    /**
     * 生成Zip
     * @param config 配置
     */
    void generatorZip(CodeGeneratorConfig config, ZipOutputStream zipOutputStream) throws IOException;
}
