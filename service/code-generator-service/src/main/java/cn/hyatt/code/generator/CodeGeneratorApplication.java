package cn.hyatt.code.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeGeneratorApplication.class, args);
        System.out.println("*****************************");
        System.out.println("CodeGenerator (代码生成器模块)");
        System.out.println("*****************************");
    }
}
