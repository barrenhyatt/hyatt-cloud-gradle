package cn.hyatt.code.generator.data;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TableItem {
    @NotBlank
    @Schema(description = "旧表名-数据库表名")
    private String oldTableName;

    @NotBlank
    @Schema(description = "表名")
    private String tableName;

    @NotBlank
    @Schema(description = "表备注")
    private String tableRemark;
}
