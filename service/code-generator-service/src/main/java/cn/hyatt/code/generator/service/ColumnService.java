package cn.hyatt.code.generator.service;

import cn.hyatt.code.generator.entity.Column;

import java.util.List;

public interface ColumnService {
    /**
     * 按表名查找全部
     * @param tableName 表名
     * @return 结果
     */
    List<Column> findAllByTableName(String databaseName,String tableName);
}
