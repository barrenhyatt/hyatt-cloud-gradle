package cn.hyatt.code.generator.mapper;

import cn.hyatt.code.generator.entity.Column;
import jakarta.validation.constraints.NotBlank;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ColumnMapper{

    /**
     * 按表名查找全部
     * @param databaseName 数据源名称
     * @param tableName 表名
     * @return 结果
     */
    List<Column> findAllByTableName(@NotBlank @Param("databaseName") String databaseName, @NotBlank @Param("tableName") String tableName);
}
