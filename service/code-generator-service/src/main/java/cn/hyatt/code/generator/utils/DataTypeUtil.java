package cn.hyatt.code.generator.utils;


public class DataTypeUtil {
    /**
     * mysql数据类型转Java数据类型
     *
     * @param dataType mysql数据类型
     * @return Java数据类型
     */
    public static String mysqlDataTypeToJavaDataType(String dataType) {
        return switch (dataType) {
            case "char", "varchar", "text" -> "String";
            case "bigint" -> "Long";
            case "int" -> "Integer";
            case "float" -> "Float";
            case "double" -> "Double";
            case "tinyint" -> "Boolean";
            case "date" -> "LocalDate";
            case "time" -> "LocalTime";
            case "datetime", "timestamp" -> "LocalDateTime";
            case "decimal" -> "BigDecimal";
            default -> "";
        };
    }
}
