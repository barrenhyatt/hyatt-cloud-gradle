package cn.hyatt.code.generator.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
public class CodeGeneratorConfig implements Serializable {
    @Serial
    private static final long serialVersionUID = 42L;

    @Schema(description = "作者")
    private String author;

    @Schema(description = "包地址")
    private String packagePath;

    @Schema(description = "表名")
    private String tableName;

    @Schema(description = "表名前缀")
    private String tablePrefix;

    @Schema(description = "表描述")
    private String tableRemark;

    @Schema(description = "uri")
    private String uri;

}
