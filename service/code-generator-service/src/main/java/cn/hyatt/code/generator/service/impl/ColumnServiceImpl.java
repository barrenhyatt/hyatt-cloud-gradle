package cn.hyatt.code.generator.service.impl;

import cn.hyatt.code.generator.entity.Column;
import cn.hyatt.code.generator.mapper.ColumnMapper;
import cn.hyatt.code.generator.service.ColumnService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@Primary
@AllArgsConstructor
public class ColumnServiceImpl implements ColumnService {
    private ColumnMapper baseMapper;
    @Override
    public List<Column> findAllByTableName(String databaseName,String tableName) {
        return baseMapper.findAllByTableName(databaseName,tableName);
    }
}
