package cn.hyatt.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties("hyatt.auth.jwt")
public class JwtProperties {

    /**
     * token 颁发签名
     */
    private String issuer = "hyatt";

    /**
     * 密钥
     */
    private String secretKey = "7ffc89883e17187f168868ccd30bd16d";

    /**
     * Token 生成后有效时长（毫秒）
     */
    private Long validDuration = 1800000L; // 默认 30 分钟

    /**
     * Token 生成后刷新token有效时长（毫秒）刷新时长要大于有效时长
     */
    private Long refreshDuration = 3600000L; // 默认 60 分钟
}
