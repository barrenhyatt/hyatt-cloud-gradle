package cn.hyatt.controller;

import cn.hyatt.dto.AuthInfo;
import cn.hyatt.dto.R;
import cn.hyatt.dto.UsernameLogin;
import cn.hyatt.service.AuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

@Slf4j
@Tag(name = "认证", description = "认证")
@ResponseBody
@RestController
@RequestMapping
@AllArgsConstructor
public class AuthController {

    private AuthService authenticationService;


    @GetMapping("/getCaptchaImage")
    @Operation(summary = "获取验证码图片")
    public ResponseEntity<InputStreamResource> getCaptchaImage(HttpServletRequest request) {
        ByteArrayOutputStream outputStream = authenticationService.getCaptcha(request);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);

        // 创建InputStreamResource对象并返回
        InputStreamResource resource = new InputStreamResource(new ByteArrayInputStream(outputStream.toByteArray()));
        return ResponseEntity.ok()
                .headers(headers)
                .body(resource);
    }

    @GetMapping("/getCaptchaBase64Image")
    @Operation(summary = "获取验证码Base64图片")
    public R<String> getCaptchaBase64Image(HttpServletRequest request) {
        return R.ok(Base64.encodeBase64String(authenticationService.getCaptcha(request).toByteArray()));
    }

    @GetMapping("/login/username")
    @Operation(summary = "用户名登录")
    public R<AuthInfo> usernameLogin(HttpServletRequest request, @RequestBody UsernameLogin login) {
        return R.ok(authenticationService.usernameLogin(request, login));
    }

    @PostMapping("/auth/refresh")
    @Operation(summary = "刷新认证")
    public R<AuthInfo> refreshAuth() {
        return R.ok(authenticationService.refreshAuth());
    }


}
