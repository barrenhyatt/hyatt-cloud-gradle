package cn.hyatt.feign;


import cn.hyatt.service.AuthService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统部门远程 Feign 调用实现
 *
 * @author hyatt
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping("/auth/feign")
@AllArgsConstructor
public class AuthFeignImpl implements AuthFeign {

    private AuthService authenticationService;

}
