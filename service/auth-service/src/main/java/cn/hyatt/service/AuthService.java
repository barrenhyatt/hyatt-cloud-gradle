package cn.hyatt.service;

import cn.hyatt.dto.AuthInfo;
import cn.hyatt.dto.UsernameLogin;
import jakarta.servlet.http.HttpServletRequest;

import java.io.ByteArrayOutputStream;

public interface AuthService {
    /**
     * 获取验证码
     * @param request 请求对象
     * @return 结果
     */
    ByteArrayOutputStream getCaptcha(HttpServletRequest request);

    /**
     * 用户名登录
     * @param login 登录信息
     * @return 结果
     */
    AuthInfo usernameLogin(HttpServletRequest request, UsernameLogin login);

    /**
     * 刷新认证
     * @return AuthInfo
     */
    AuthInfo refreshAuth();
}
