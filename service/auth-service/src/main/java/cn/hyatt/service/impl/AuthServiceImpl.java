package cn.hyatt.service.impl;

import cn.hyatt.config.properties.JwtProperties;
import cn.hyatt.constant.SystemConfigConstant;
import cn.hyatt.dto.AuthInfo;
import cn.hyatt.dto.UsernameLogin;
import cn.hyatt.dto.systemConfig.SystemConfigVo;
import cn.hyatt.feign.SystemConfigFeign;
import cn.hyatt.security.entity.LoginUserDetails;
import cn.hyatt.service.AuthService;
import cn.hyatt.utils.JwtUtils;
import cn.hyatt.utils.KaptchaUtils;
import com.alibaba.fastjson.JSONObject;
import com.google.code.kaptcha.Producer;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class AuthServiceImpl implements AuthService {
    // Redis 验证码缓存名称
    private static final String CAPTCHA_KEY = "system:auth:captcha:";
    private SystemConfigFeign systemConfigFeign;
    private RedissonClient redissonClient;
    private JwtProperties jwtProperties;
    private AuthenticationManager authenticationManager;

    @SneakyThrows
    @Override
    public ByteArrayOutputStream getCaptcha(HttpServletRequest request) {
        // 获取请求用户Session信息
        HttpSession session = request.getSession();
        String sessionId = session.getId();
        // 生成验证码内容
        Optional<SystemConfigVo> captchaLengthOpt = systemConfigFeign.findByCode(SystemConfigConstant.CAPTCHA_LENGTH);
        Integer captchaLength = captchaLengthOpt.map(systemConfigVo -> Integer.parseInt(systemConfigVo.getValue())).orElse(5);
        Producer producer = KaptchaUtils.buildProducer(captchaLength);
        String captcha = producer.createText();
        // 保存验证码内容
        saveCaptcha(sessionId, captcha);
        // 生成验证码图片
        BufferedImage captchaImage = producer.createImage(captcha);
        // 转换为 Output
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(captchaImage, "png", outputStream);
        return outputStream;
    }

    /**
     * 保存验证码信息
     * @param sessionId SessionId
     * @param captcha 验证码
     */
    protected void saveCaptcha(String sessionId,String captcha) {
        RBucket<String> bucket = redissonClient.getBucket(CAPTCHA_KEY + sessionId);
        bucket.set(captcha);
    }

    /**
     * 获取验证码
     * @param sessionId SessionId
     * @return 验证码
     */
    protected String getCaptcha(String sessionId) {
        RBucket<String> bucket = redissonClient.getBucket(CAPTCHA_KEY + sessionId);
        if (bucket.isExists()) {
            return bucket.get();
        } else {
            throw new RuntimeException("获取不到验证码信息,无法确认验证真伪,请重新登录认证");
        }
    }

    @Override
    public AuthInfo usernameLogin(HttpServletRequest request,UsernameLogin login) {
        // 验证码验证
        captchaVerify(request, login.getCaptcha());
        // 获取用户信息
        // 封装 SpringSecurity 验证响应请求
        Authentication authenticationRequest = UsernamePasswordAuthenticationToken
                .unauthenticated(login.getUsername(), login.getPassword());
        // 身份验证响应
        Authentication authenticationResponse = authenticationManager.authenticate(authenticationRequest);
        // 登录用户详细信息
        LoginUserDetails userDetails = (LoginUserDetails) authenticationResponse.getPrincipal();
        return createAuthInfo(userDetails);
    }

    @Override
    public AuthInfo refreshAuth() {
        return null;
    }

    /**
     * 创建认证信息
     * @param userDetails 用户详情
     * @return AuthInfo
     */
    private AuthInfo createAuthInfo(LoginUserDetails userDetails) {
        // 创建 Token
        String json = JSONObject.toJSONString(userDetails);
        Map<String, Object> map = JSONObject
                .parseObject(json)
                .getInnerMap();
        map.put("issuer", jwtProperties.getIssuer());
        map.remove("password");

        // 创建Token 与 刷新Token
        String accessToken = JwtUtils.createJWT(jwtProperties.getSecretKey(), jwtProperties.getValidDuration(), map);
        String refreshToken = JwtUtils.createJWT(jwtProperties.getSecretKey(), jwtProperties.getRefreshDuration(), map);
        AuthInfo authInfo = new AuthInfo();
        authInfo.setAccessToken(accessToken);
        authInfo.setRefreshToken(refreshToken);
        return authInfo;
    }

    /**
     * 验证码验证
     * @param request 请求对象
     * @param captcha 用户输入的验证码
     */
    protected void captchaVerify(HttpServletRequest request,String captcha) {
        // 获取是否校验验证码配置
        Optional<SystemConfigVo> isEnableCaptchaVerifyConfigOpt = systemConfigFeign.findByCode(SystemConfigConstant.IS_ENABLE_CAPTCHA_VERIFY);
        Boolean isEnableCaptchaVerify = isEnableCaptchaVerifyConfigOpt.map(systemConfigVo -> Boolean.parseBoolean(systemConfigVo.getValue())).orElse(true);
        // 是否校验验证码
        if (isEnableCaptchaVerify) {
            // 获取是否区分大小写验证码配置
            Optional<SystemConfigVo> isCaseSensitiveCaptchaConfigOpt = systemConfigFeign.findByCode(SystemConfigConstant.IS_CASE_SENSITIVE_CAPTCHA);
            Boolean isCaseSensitiveCaptcha = isCaseSensitiveCaptchaConfigOpt.map(systemConfigVo -> Boolean.parseBoolean(systemConfigVo.getValue())).orElse(true);
            // 获取请求用户Session信息
            HttpSession session = request.getSession();
            String sessionId = session.getId();
            String verifyCaptcha = getCaptcha(sessionId);
            if (isCaseSensitiveCaptcha) {
                if (!verifyCaptcha.equals(captcha)) {
                    throw new RuntimeException("验证码错误");
                }
            } else {
                verifyCaptcha = verifyCaptcha.toLowerCase();
                if (!verifyCaptcha.equals(captcha.toLowerCase())) {
                    throw new RuntimeException("验证码错误");
                }
            }
        }
    }


}
