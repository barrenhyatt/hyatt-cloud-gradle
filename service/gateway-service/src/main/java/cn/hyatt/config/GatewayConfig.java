package cn.hyatt.config;

import cn.hyatt.config.properties.RouteProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationPropertiesScan(basePackageClasses = {RouteProperties.class})
public class GatewayConfig {

}
