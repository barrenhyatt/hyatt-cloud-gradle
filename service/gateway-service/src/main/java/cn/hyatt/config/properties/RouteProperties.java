package cn.hyatt.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * 路由属性
 *
 * @author hyatt
 */
@Getter
@Setter
@ConfigurationProperties("hyatt.route")
public class RouteProperties {
    /**
     * 白名单
     * 不需要认证就可以访问的路由地址
     */
    private List<String> whiteList;

    /**
     * 黑名单
     * 无论是否认证都限制访问的路由地址
     */
    private List<String> blackList;

    /**
     * 身份验证请求头
     */
    private String AuthHeader = "Auth-Token";

    /**
     * 令牌前缀
     */
    private String TokenPrefix = "Bearer ";
}
