package cn.hyatt.filter;

import cn.hyatt.config.properties.RouteProperties;
import cn.hyatt.utils.StringTool;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.Optional;

/**
 * 全局过滤器
 * 全局过滤器可以用于一些全局的逻辑处理，例如日志记录、全局异常处理等。
 * @author hyatt
 */
@Slf4j
@Component
@AllArgsConstructor
public class AuthenticationFilter implements GlobalFilter, Ordered {

    private RouteProperties routeProperties;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        log.debug(this.getClass().getName() + ":AuthenticationFilter(认证过滤)：" + request.getPath().value());
        // 路由-黑名单
        if (routeProperties.getBlackList().contains(request.getPath().value())) {
            throw new RuntimeException(this.getClass().getName() + ":该请求地址，已被开发者设为不可访问");
        }
        // 路由-白名单
        if (routeProperties.getWhiteList().contains(request.getPath().value())) {
            return chain.filter(exchange);
        }
        // 获取认证信息
        Optional<String> authHeader = Objects.requireNonNull(request.getHeaders().get(routeProperties.getAuthHeader())).stream().findFirst();
        String token = authHeader.orElseThrow(() -> new RuntimeException(this.getClass().getName() + ":无法获取到认证信息"));
        // token
        // 判断认证信息是否符合
        String tokenPrefix = routeProperties.getTokenPrefix();
        if (token.startsWith(tokenPrefix)) {
            token = StringTool.removePrefix(token, tokenPrefix);
            //Boolean verify = authFeign.tokenVerify(token);
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
