package cn.hyatt.listener;

import com.alibaba.nacos.client.naming.event.InstancesChangeEvent;
import com.alibaba.nacos.common.notify.Event;
import com.alibaba.nacos.common.notify.listener.Subscriber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

/**
 * 实例更改事件侦听器
 * Nacos 实例更改事件监听
 * @author hyatt
 */
@Slf4j
@Configuration
public class InstancesChangeEventListener extends Subscriber<InstancesChangeEvent> {
    @Override
    public void onEvent(InstancesChangeEvent event) {
        System.out.println("*************************");
        System.out.println("Nacos 实例更改事件监听");

        System.out.println("服务名："+event.getServiceName());
        System.out.println("组名："+event.getGroupName());
        System.out.println("集群："+event.getClusters());

        System.out.println("*************************");

    }

    @Override
    public Class<? extends Event> subscribeType() {
        return null;
    }
}
