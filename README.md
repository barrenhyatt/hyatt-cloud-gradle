# hyatt-cloud-gradle

## 介绍

项目为分布式项目，采用 SpringCloudAlibaba 搭建。



## 软件架构

### 模块介绍

#### common(能力)

该模块主要为后续开发扩展提供便携，为常见功能进行封装，为模块所使用到的功能提供便携。该模块不涉及业务功能。    

#### api(模块公共资源)

该模块为可运行服务的静态资源模块，为后续模块之间引用提供便携，该模块主要存放,dto,vo,enum,event,feign等资源类。

#### common(常见工具模块)

该模块为工具模块，例如常用的SpringUtils等。

#### service(服务)

该模块为可运行业务模块，主要提供对应业务功能。

### 项目版本介绍

#### 运行代码所需应用说明

Gradle + JDK17 + Mysql + Redis + Nacos

> Mysql Redis Nacos 由云服务器提供，配置无需修改

#### Spring 版本说明
SpringBoot-v3.2.3

SpringCloud-v2023.0.0

SpringCloudAlibaba-v2023.0.0.0-RC1



## 安装教程

无



## 使用说明

无



## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
